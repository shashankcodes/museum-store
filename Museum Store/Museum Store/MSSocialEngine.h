//
//  MSFacebookEngine.h
//  Museum Store
//
//  Created by shanks on 16/02/14.
//  Copyright (c) 2014 shashankCodes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSSocialEngine : NSObject

- (void) syncAndUnsycFacebookAccountWithCompletionBlock:(void(^)(BOOL status)) completion;

@end
