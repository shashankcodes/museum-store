//
//  MSAnnotationView.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "MSAnnotation.h"

@interface MSAnnotationView : MKAnnotationView
@property (strong, nonatomic) MSAnnotation *museumAnnotation;

@end
