//
//  MSBeaconDetector.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSBeaconDetector.h"
#import <CoreLocation/CoreLocation.h>
#import "MSBeacon.h"
#import "MSAppDelegate.h"
#import "SCViewController.h"
#import "MSPurchaserTabbarController.h"
#import "MSPurchaserAllItemsViewController.h"
#import "MSPurchaserStoreViewController.h"
#import "MSPurchaserMapViewController.h"
#import "MSTabbarViewController.h"
#import "MSCollectorGameViewController.h"
#import "MSCollectorExhibitViewController.h"
#import "MSCollectorMapViewController.h"
#import "MSProfileViewController.h"


@interface MSBeaconDetector() <CLLocationManagerDelegate>{
    NSString* foundMuseum;
    NSString* foundStore;
}

@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSArray *validBeaconsArray;


@end

@implementation MSBeaconDetector

+(MSBeaconDetector *)SharedInstance
{
    static MSBeaconDetector *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MSBeaconDetector alloc] init];
        sharedInstance.basketArray = [[NSMutableArray alloc]init];
        [sharedInstance initiate];
    });
    return sharedInstance;

}


- (void)initiate
{
        
    // Do any additional setup after loading the view from its nib.
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MuseumConfigure" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    self.validBeaconsArray = [dict objectForKey:@"beacons"];
    NSDictionary *museumDict = [dict objectForKey:@"museum"];
    
    // Initialize location manager and set ourselves as the delegate
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // Create a NSUUID with the same UUID as the broadcasting beacon
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:[museumDict objectForKey:@"UUID"]];
    // Setup a new region with that UUID and same identifier as the broadcasting beacon
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                             identifier:@"Estimote Region"];
    
    
    self.myBeaconRegion.notifyEntryStateOnDisplay = YES;
    // Tell location manager to start monitoring for the beacon region
    [self.locationManager startMonitoringForRegion:self.myBeaconRegion];
}
-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}
-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region{
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}
- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    [self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
}

-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region
{
      MSAppDelegate *appDel = (MSAppDelegate *)[UIApplication sharedApplication].delegate ;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MuseumConfigure" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSDictionary *museumDict = [dict objectForKey:@"museum"];
    NSDictionary *storeDict = [dict objectForKey:@"storeBeacon"];
    
    foundMuseum = [[NSUserDefaults standardUserDefaults]  objectForKey:@"foundMuseum"];
    foundStore = [[NSUserDefaults standardUserDefaults]  objectForKey:@"foundStore"];
    
    // Beacon found!
    for(CLBeacon *foundBeacon in beacons)
    {
        
       if ((foundBeacon.proximity == CLProximityImmediate)||(foundBeacon.proximity == CLProximityNear))
       {
           if (self.currentUser)
           {
               
               
               if ([self isValidBeacon:foundBeacon])
               {
                   NSString *uuidString = [NSString stringWithFormat:@"%@",foundBeacon.proximityUUID.UUIDString];
                   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"major == %@ && minor == %@",foundBeacon.major.stringValue ,foundBeacon.minor.stringValue];
                   
                
                   NSSet *filteredSet = [self.currentUser.beacons filteredSetUsingPredicate:predicate];
                   if (!filteredSet.count)
                   {
                       MSBeacon *beacon =[NSEntityDescription insertNewObjectForEntityForName:@"MSBeacon" inManagedObjectContext:appDel.managedObjectContext];
                       beacon.major = [foundBeacon.major stringValue];
                       beacon.minor = [foundBeacon.minor stringValue];
                       beacon.uuid = uuidString;
                       
                       beacon.user = self.currentUser;
                       
                       
                       UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:@"You have just earned 20 points for the new founded beacon" delegate:nil cancelButtonTitle:@"Great! Thanks" otherButtonTitles:nil];
                       [alertView show];
                       
                       [[NSNotificationCenter defaultCenter] postNotificationName:@"beaconNotification" object:nil];
                       
                   }
                   NSError *error;
                   [appDel.managedObjectContext save:&error];
                   
               }
             else if ([museumDict[@"major"] isEqualToNumber:foundBeacon.major] && [museumDict[@"minor"] isEqualToNumber:foundBeacon.minor])
             {
                 
                 
                 if(![foundMuseum isEqual: @"yes"]){
                     foundMuseum = @"yes";
                     [[NSUserDefaults standardUserDefaults]setObject:foundMuseum forKey:@"foundMuseum"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     RESideMenu *sideMenu = (RESideMenu *)appDel.window.rootViewController;
                     {
                         //    MSCollectorViewController *collectorController = [[MSCollectorViewController alloc] initWithNibName:@"MSCollectorViewController" bundle:nil];
                         MSTabbarViewController *tabBarController = [[MSTabbarViewController alloc] initWithNibName:@"MSTabbarViewController" bundle:nil];
                         
                         MSCollectorGameViewController *collectorGameViewController = [[MSCollectorGameViewController alloc] initWithNibName:@"MSCollectorGameViewController" bundle:nil];
                         UINavigationController *navRootControllerGame = [[UINavigationController alloc] initWithRootViewController:collectorGameViewController];
                         
                         MSCollectorExhibitViewController *collectorExhibitViewController = [[MSCollectorExhibitViewController alloc] initWithNibName:@"MSCollectorExhibitViewController" bundle:nil];
                         UINavigationController *navRootControllerExhibit = [[UINavigationController alloc] initWithRootViewController:collectorExhibitViewController];
                         MSCollectorMapViewController *collectorMapViewController = [[MSCollectorMapViewController alloc] initWithNibName:@"MSCollectorMapViewController" bundle:nil];
                         UINavigationController *navRootControllerMap = [[UINavigationController alloc] initWithRootViewController:collectorMapViewController];
                         
                         MSProfileViewController *profileViewController = [[MSProfileViewController alloc] initWithNibName:@"MSProfileViewController" bundle:nil];
                         UINavigationController *navRootControllerProfile = [[UINavigationController alloc] initWithRootViewController:profileViewController];
                         
                         UIViewController *viewController = [[UIViewController alloc] init];
                         
                         [tabBarController setViewControllers:@[navRootControllerProfile,navRootControllerGame,navRootControllerExhibit,navRootControllerMap,[[UIViewController alloc] init],[[UIViewController alloc] init]]];
                         tabBarController.selectedIndex = 1;
                         [sideMenu setContentViewController:tabBarController animated:YES];
                         [sideMenu hideMenuViewController];
                         
                         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Museum Store" message:@"Welcome to Lorum Ipsum Museum" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [alertView show];
                 }
                 
                     
                 }
                 
                 
             }
             else if ([storeDict[@"major"] isEqualToNumber:foundBeacon.major] && [storeDict[@"minor"] isEqualToNumber:foundBeacon.minor])
             {
                 if(![foundStore isEqual: @"yes"]){
                     foundStore = @"yes";
                     [[NSUserDefaults standardUserDefaults]setObject:foundStore forKey:@"foundStore"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                      RESideMenu *sideMenu = (RESideMenu *)appDel.window.rootViewController;
                     MSPurchaserTabbarController *tabBarController = [[MSPurchaserTabbarController alloc] init];
                     
                     
                     MSPurchaserStoreViewController *purchaserStoreViewController = [[MSPurchaserStoreViewController alloc] initWithNibName:@"MSPurchaserStoreViewController" bundle:nil];
                     UINavigationController *navRootPurchaserStore = [[UINavigationController alloc] initWithRootViewController:purchaserStoreViewController];
                     
                     MSCollectorMapViewController *collectorMapViewController = [[MSCollectorMapViewController alloc] initWithNibName:@"MSCollectorMapViewController" bundle:nil];
                     UINavigationController *navRootControllerMap = [[UINavigationController alloc] initWithRootViewController:collectorMapViewController];
                     
                     MSPurchaserAllItemsViewController *purchaserAllItemsViewController = [[MSPurchaserAllItemsViewController alloc] initWithNibName:@"MSPurchaserAllItemsViewController" bundle:nil];
                     UINavigationController *navRootPurchaserAllItems = [[UINavigationController alloc] initWithRootViewController:purchaserAllItemsViewController];
                     
                     SCViewController *scanViewController = [[SCViewController alloc] init];
                     UINavigationController *navRootPurchaserQRScan = [[UINavigationController alloc] initWithRootViewController:scanViewController];
                     
                     [tabBarController setViewControllers:@[[[UIViewController alloc] init],navRootPurchaserStore,navRootControllerMap,navRootPurchaserAllItems,navRootPurchaserQRScan,[[UIViewController alloc] init]]];
                     tabBarController.selectedIndex = 1;
                     [sideMenu setContentViewController:tabBarController animated:YES];
                     [sideMenu hideMenuViewController];
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Museum Store" message:@"Welcome to the Store" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alertView show];
                     
                     
                 }
             }
               
               
               
           }
       }
    }
}

-(BOOL) isValidBeacon:(CLBeacon *)beacon
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"major == %@ && minor == %@",beacon.major ,beacon.minor];
    
    NSArray *filteredArray = [self.validBeaconsArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count >0)
    {
        id obj = filteredArray[0];
        
       NSInteger index = [self.validBeaconsArray indexOfObject:obj];
        
        if (!self.detectedBeaconIndexesArray)
        {
            
            self.detectedBeaconIndexesArray = [NSMutableArray array];
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];

            for (MSBeacon *beaconCore in self.currentUser.beacons)
            {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"major == %@ && minor == %@",[f numberFromString:beaconCore.major],[f numberFromString:beaconCore.minor]];
                filteredArray = [self.validBeaconsArray filteredArrayUsingPredicate:predicate];
                id obj1 = filteredArray[0];
                NSInteger index1 = [self.validBeaconsArray indexOfObject:obj1];                
                [self.detectedBeaconIndexesArray addObject:@(index1)];
                
            }
        }
        
        if (![self.detectedBeaconIndexesArray containsObject:@(index)])
        {
            [self.detectedBeaconIndexesArray addObject:@(index)];
        }
    }
    
    return (filteredArray.count>0);
}


@end
