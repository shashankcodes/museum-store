//
//  MSLoginManager.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSLoginManager.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "UIAlertView+Blocks.h"
#import "MSAppDelegate.h"
#import "SVProgressHUD.h"



@interface MSLoginManager ()<UIActionSheetDelegate>

@property (strong, nonatomic) NSArray *accountsArray;

@property (copy, nonatomic) LoginCompletionBlock twitterSuccessBlock;
@property (copy ,nonatomic) LoginCompletionBlock twitterFailureBlock;

@end



@implementation MSLoginManager



- (void) fbLoginWithsuccessBlock:(LoginCompletionBlock)successBlock FailureBlock:(LoginCompletionBlock) FailureBlock
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MuseumConfigure" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    NSString *appID =[[dict objectForKey:@"faceBook"] objectForKey:@"appId"];
    
    NSString *urlString = [[dict objectForKey:@"faceBook"] objectForKey:@"url"];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
       // [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *facebookAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        
        NSDictionary *options = @{
                                  @"ACFacebookAppIdKey" : appID,
                                  ACFacebookPermissionsKey:@[@"email"]}; // Needed only when write permissions are requested
        
        [accountStore requestAccessToAccountsWithType:facebookAccountType options:options completion:^(BOOL granted, NSError *error)
         {
             if (granted)
             {
                 NSArray *accountsArray = [accountStore accountsWithAccountType:facebookAccountType];
                 
                 ACAccount *account = [accountsArray objectAtIndex:0];
                 
                 NSLog(@"Token: %@", account.credential.oauthToken);
                 if (!account.credential.oauthToken)
                 {
                 //    [SVProgressHUD dismissWithError:@"Fail"];
                 }
                 
                 
                 NSURL *requestURL = [NSURL URLWithString:urlString];
                 NSString *serviceType = SLServiceTypeFacebook;
                 
                 NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                 
                 
                 [queue setName:@"Perform request"];
                 [queue addOperationWithBlock:^{
                     
                     SLRequest *request = [SLRequest requestForServiceType:serviceType requestMethod:SLRequestMethodGET URL:requestURL parameters:nil];
                     
                     [request setAccount:account];
                     
                     
                     [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
                         
                         // Handle the response...
                         if (error) {
                             if (FailureBlock) {
                                 NSString *errorMsg = [NSString stringWithFormat:@"%@",error.localizedDescription];
                                 NSError *error =[NSError errorWithDomain:@"Facebook" code:-9999 userInfo:@{@"message":errorMsg}];
                                 FailureBlock(error);
                             }
                             NSLog(@"Error: %@", error);
                             //Handle error
                            // [SVProgressHUD dismissWithError:@"fail"];
                             
                         }
                         else {
                             
                             NSDictionary* jsonResults = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                             
                             if (error) {
                                 if (FailureBlock) {
                                     NSString *errorMsg = [NSString stringWithFormat:@"%@",error.localizedDescription];
                                     NSError *error =[NSError errorWithDomain:@"Facebook" code:-9999 userInfo:@{@"message":errorMsg}];
                                     FailureBlock(error);
                                 }
                                 NSLog(@"Error: Error serializating object");
                                 //Handle error
                                // [SVProgressHUD dismissWithError:@"fail"];
                             
                             }
                             else {
                                 
                                 NSDictionary *errorDictionary = [jsonResults valueForKey:@"error"];
                                 
                                 if (errorDictionary) {
                                     NSString *errorMsg = [NSString stringWithFormat:@"%@",error.localizedDescription];
                                     NSError *error =[NSError errorWithDomain:@"Facebook" code:-9999 userInfo:@{@"message":errorMsg}];
                                     FailureBlock(error);
                                   //  [SVProgressHUD dismissWithError:@"fail"];
                                 }
                                 else {
                                     [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                         NSLog(@"jsonResults: %@", jsonResults);
                                        
                                         NSString *email = [jsonResults objectForKey:@"email"];
                                         NSString *firstname = [jsonResults objectForKey:@"first_name"];
                                         NSString *lastName = [jsonResults objectForKey:@"last_name"];
                                         
                                         
                                         NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
                                         
                                         [userInfo setObject:email forKey:kEmailAddress];
                                         if (firstname)
                                         {
                                             [userInfo setObject:firstname forKey:kFirstName];
                                            
                                         }
                                         
                                         if (lastName) {
                                             [userInfo setObject:lastName forKey:kLastName];
                                         }
                                         
                                         if (successBlock) {
                                             successBlock(userInfo);
                                         }
                                         NSLog(@"createUser :%@", userInfo);
//                                         [self createUser:userInfo];
                                     }];
                                 }
                             }
                         }
                     }];
                 }];
                 
             }else
             {
                 NSLog(@"%@",error);
                 if (FailureBlock) {
                     NSString *errorMsg = [NSString stringWithFormat:@"%@",error.localizedDescription];
                     NSError *error =[NSError errorWithDomain:@"Facebook" code:-9999 userInfo:@{@"message":errorMsg}];
                     FailureBlock(error);
                 }
                // [SVProgressHUD dismiss];
                 
             }
             
         }];
        
    }else
    {
        if (FailureBlock) {
            NSError *error =[NSError errorWithDomain:@"Facebook" code:-10001 userInfo:@{@"message":@"Facebook not Configured"}];
            FailureBlock(error);
        }
    }
}


- (void) twitterLoginWithsuccessBlock:(LoginCompletionBlock)successBlock FailureBlock:(LoginCompletionBlock) FailureBlock
{
    
    self.twitterFailureBlock = FailureBlock;
    self.twitterSuccessBlock = successBlock;
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
       // [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *twitterAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        [accountStore requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
            if(granted) {
                
                NSArray *accountsArray = [accountStore accountsWithAccountType:twitterAccountType];
                if (!accountsArray.count)
                {
                    UIAlertView *twitterAlert = [[UIAlertView alloc] initWithTitle:@"Twitter Account Error" message:@"Please check Accounts" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [twitterAlert show];
                    return;
                }
                
                self.accountsArray = accountsArray;
                
                
                if (accountsArray.count>1)
                {
                    NSArray *usernamesArray = [accountsArray valueForKey:@"username"];
                    
                    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Twitter Account"
                                                                             delegate:self
                                                                    cancelButtonTitle:nil
                                                               destructiveButtonTitle:nil
                                                                    otherButtonTitles:nil];
                    
                    // ObjC Fast Enumeration
                    for (NSString *title in usernamesArray) {
                        [actionSheet addButtonWithTitle:title];
                    }
                    
                    [actionSheet addButtonWithTitle:@"Cancel"];
                    actionSheet.cancelButtonIndex = [usernamesArray count];
 
                   
                    
                    MSAppDelegate *appDel = (MSAppDelegate *)[[UIApplication sharedApplication] delegate];
                    dispatch_async(dispatch_get_main_queue(), ^{
                           [actionSheet showInView:appDel.window];
                    });
                 

                }
                else
                {
                   
                    [self performTwitterLoginWithIndex:0];
                }
                
                
     
        
    }else
    {
        if (FailureBlock) {
            
            NSError *error =[NSError errorWithDomain:@"Twitter" code:-10001 userInfo:@{@"message":@"Twitter Not Configured"}];
            FailureBlock(error);
        }
    }
        }];
    }
    
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex <self.accountsArray.count) {
        [self performTwitterLoginWithIndex:buttonIndex];
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    }
}

-(void) performTwitterLoginWithIndex:(NSInteger)buttonIndex
{
    [SVProgressHUD showWithStatus:@"Signing In" maskType:SVProgressHUDMaskTypeClear];
    
    NSArray *accountsArray = self.accountsArray;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MuseumConfigure" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];

    NSString *urlString = [dict[@"twitter"] objectForKey:@"url"];
    
    ACAccount *twitterAccount = [self.accountsArray objectAtIndex:buttonIndex];
    
    NSLog(@"twitter account info :%@", twitterAccount.username);
    NSMutableDictionary *twitterHandleInfo = [[NSMutableDictionary alloc] init];
    [twitterHandleInfo setObject:twitterAccount.accountDescription forKey:kHandleKey];
    [twitterHandleInfo setObject:@"Twitter" forKey:kHandleType];
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:twitterHandleInfo forKey:kUserHandle];
    
    
    NSString *url = urlString;
    SLRequest *request =  [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:[NSURL URLWithString:url] parameters:nil];
    [request setAccount:[accountsArray firstObject]];
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData) {
            // [SVProgressHUD dismissWithSuccess:@"success"];
            if (urlResponse.statusCode >= 200 &&
                urlResponse.statusCode < 300) {
                
                NSError *jsonError;
                NSDictionary *timelineData =
                [NSJSONSerialization
                 JSONObjectWithData:responseData
                 options:NSJSONReadingAllowFragments error:&jsonError];
                if (timelineData) {
                  
                    NSString *name = [timelineData objectForKey:@"name"];
                    NSArray *userNames = [name componentsSeparatedByString:@" "];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (userNames.count == 2)
                        {
                            [userInfo setObject:[userNames objectAtIndex:0] forKey:kFirstName];
                            [userInfo setObject:[userNames objectAtIndex:1] forKey:kLastName];
                        }else if (userNames.count == 1)
                        {
                            [userInfo setObject:[userNames objectAtIndex:0] forKey:kFirstName];
                            [userInfo setObject:@"" forKey:kFirstName];
                            
                        }
                        [userInfo setObject:[NSString stringWithFormat:@"@%@", twitterAccount.username] forKey:kEmailAddress];
                        if (self.twitterSuccessBlock)
                        {
                            self.twitterSuccessBlock(userInfo);
                        }
                        
                        
                    });
                    
                }
                else {
                    // Our JSON deserialization went awry
                    NSLog(@"JSON Error: %@", [jsonError localizedDescription]);
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.twitterFailureBlock) {
                            NSString *errorMsg = [NSString stringWithFormat:@"%@",jsonError.localizedDescription];
                            NSError *error =[NSError errorWithDomain:@"Twitter" code:-9999 userInfo:@{@"message":errorMsg}];
                            self.twitterFailureBlock(error);
                        }
                    });
                    //   [SVProgressHUD dismissWithError:@"error"];
                    
                    
                }
            }
            else
            {
                // The server did not respond ... were we rate-limited?
                NSLog(@"The response status code is %d",
                      urlResponse.statusCode);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.twitterFailureBlock) {
                        NSString *errorMsg = [NSString stringWithFormat:@"The response status code is %d",urlResponse.statusCode];
                        NSError *error =[NSError errorWithDomain:@"Twitter" code:-9999 userInfo:@{@"message":errorMsg}];
                        self.twitterFailureBlock(error);
                    }
                });
                // [SVProgressHUD dismissWithError:@"error"];
                
            }
        }else{
            if (self.twitterFailureBlock) {
                NSString *errorMsg = [NSString stringWithFormat:@"%@",error.localizedDescription];
                NSError *error =[NSError errorWithDomain:@"Twitter" code:-9999 userInfo:@{@"message":errorMsg}];
                self.twitterFailureBlock(error);
            }
            //[SVProgressHUD dismissWithError:@"error"];
        }
    }];
}


@end
