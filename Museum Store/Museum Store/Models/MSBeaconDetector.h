//
//  MSBeaconDetector.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSUser.h"

@interface MSBeaconDetector : NSObject

+(MSBeaconDetector *) SharedInstance;
@property (strong, nonatomic) MSUser *currentUser;
@property (strong, nonatomic) NSMutableArray *detectedBeaconIndexesArray;
@property (strong, nonatomic) NSMutableArray *basketArray;

@end
