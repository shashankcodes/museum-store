//
//  MSUser.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSUser.h"


@implementation MSUser

@dynamic userId;
@dynamic username;
@dynamic beacons;

+ (NSArray*)getAllInMOC:(NSManagedObjectContext*)moc
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"MSUser" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    
    if (array == nil) return @[];
    return array;
}


@end
