//
//  MSBeacon.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSBeacon.h"
#import "MSUser.h"


@implementation MSBeacon

@dynamic identifier;
@dynamic major;
@dynamic minor;
@dynamic uuid;
@dynamic user;

@end
