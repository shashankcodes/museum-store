//
//  MSUser.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MSUser : NSManagedObject

@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *beacons;

+ (NSArray*)getAllInMOC:(NSManagedObjectContext*)moc;


@end



@interface MSUser (CoreDataGeneratedAccessors)

- (void)addBeaconsObject:(NSManagedObject *)value;
- (void)removeBeaconsObject:(NSManagedObject *)value;
- (void)addBeacons:(NSSet *)values;
- (void)removeBeacons:(NSSet *)values;

@end
