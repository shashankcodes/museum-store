//
//  MSLoginManager.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^LoginCompletionBlock)(id response);

@interface MSLoginManager : NSObject

#define kHandleKey        @"handleKey"
#define kHandleType       @"handleType"
#define kFirstName        @"firstName"
#define kLastName         @"lastName"
#define kEmailAddress     @"emailAddress"
#define kRoles            @"roles"
#define kTwitter          @"Twitter"
#define kUserHandle       @"userHandle"
#define koAuthDetails     @"oAuthDetails"
#define kNewUserInfo      @"newUserInfo"
#define kHandleSecret     @"handleSecret"

- (void) fbLoginWithsuccessBlock:(LoginCompletionBlock)successBlock FailureBlock:(LoginCompletionBlock) FailureBlock ;
- (void) twitterLoginWithsuccessBlock:(LoginCompletionBlock)successBlock FailureBlock:(LoginCompletionBlock) FailureBlock;




@end
