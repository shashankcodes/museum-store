//
//  MSFacebookEngine.m
//  Museum Store
//
//  Created by shanks
//  Copyright (c) 2014 shashankCodes. All rights reserved.
//

#import "MSSocialEngine.h"
#import <FacebookSDK/FacebookSDK.h>

@implementation MSSocialEngine

- (void) syncAndUnsycFacebookAccountWithCompletionBlock:(void (^)(BOOL))completion
{
    if (![FBSession.activeSession isOpen])
    {
        [FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
        {
            if (error) {
                if (completion) {
                    completion(NO);
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"User cancel the permission so please enable the permission device settings" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }
            else {
                if ([[session accessTokenData] accessToken])
                {
                    [FBSession setActiveSession:session];
                    NSLog(@"Facebook account successsfly sync :%@",[FBSession setActiveSession:session]);                    
                    if (completion) {
                        completion(YES);
                    }
                    
                }else{
                    completion(NO);
                }
            }

        }];
        /*
        [FBSession openActiveSessionWithPublishPermissions: [NSArray arrayWithObjects: @"publish_stream", nil]
                                           defaultAudience: FBSessionDefaultAudienceEveryone
                                              allowLoginUI: YES
         
                                         completionHandler: ^(FBSession *session,
                                                              FBSessionState status,
                                                              NSError *error)
         {
             if (error) {
                 if (completion) {
                     completion(NO);
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"User cancel the permission so please enable the permission device settings" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alertView show];
                 }
             }
             else {
                 if ([[session accessTokenData] accessToken])
                 {
                     [FBSession setActiveSession:session];
                     NSLog(@"Facebook account successsfly sync :%@",[FBSession setActiveSession:session]);
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Facebook account sync success" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alertView show];
                     
                     if (completion) {
                         completion(YES);
                     }
                     
                 }else{
                     completion(NO);
                 }
             }
         }];
    }
         */
    }
}


@end
