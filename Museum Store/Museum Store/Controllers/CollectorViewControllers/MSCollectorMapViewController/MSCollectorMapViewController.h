//
//  MSCollectorMapViewController.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MSCollectorMapViewController : UIViewController<MKMapViewDelegate>

@end
