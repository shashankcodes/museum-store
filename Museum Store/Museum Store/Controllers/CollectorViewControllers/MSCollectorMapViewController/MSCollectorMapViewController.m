//
//  MSCollectorMapViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSCollectorMapViewController.h"
#import <MapKit/MapKit.h>
#import "MSAnnotation.h"
#import "MSAnnotationView.h"
#import "MSHomeViewController.h"
#import "RESideMenu.h"
#import "MSPurchaserTabbarController.h"
#import "MSPurchaserStoreViewController.h"
#import "MSPurchaserAllItemsViewController.h"
#import "SCViewController.h"
#import "MSSearchViewController.h"

@interface MSCollectorMapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSMutableArray *annotationsArray;
@end

@implementation MSCollectorMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"MUSEUM WORLDWIDE";
    [self customizeNavigationBar];
    [self initializeDummyData];
}

-(void) initializeDummyData
{
    MSAnnotation *annotation1 = [[MSAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(17.22, 78.28)];
    
    MSAnnotation *annotation2 = [[MSAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(39.75, -86.14)];
     MSAnnotation *annotation3 = [[MSAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(36.82, -75.98)];
     MSAnnotation *annotation4 = [[MSAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(42.35, -71.05)];
     MSAnnotation *annotation5 = [[MSAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(39.75, -86.14)];
    
     MSAnnotation *annotation6 = [[MSAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(39.77, -86.15)];
    
    
    [self.mapView setCenterCoordinate:annotation3.coordinate];
    self.mapView.delegate = self;
      [self.mapView addAnnotations:@[annotation1,annotation2,annotation3,annotation4,annotation5,annotation6]];
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MSAnnotation class]])
    {
        static NSString *identifier = @"MuseumScore";
        MSAnnotationView *annotationView = (MSAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (!annotationView)
        {
            annotationView = [[MSAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
        }
        
        annotationView.annotation =annotation;
        
        [annotationView setImage:[UIImage imageNamed:@"pin_map.png"]];
        annotationView.museumAnnotation = annotation;
        return annotationView;
    }
    else
    {
        return nil;
    }
    
    
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[MSAnnotation class]])
    {
       
        MSPurchaserTabbarController *tabBarController = [[MSPurchaserTabbarController alloc] init];
        
        
        MSPurchaserStoreViewController *purchaserStoreViewController = [[MSPurchaserStoreViewController alloc] initWithNibName:@"MSPurchaserStoreViewController" bundle:nil];
        UINavigationController *navRootPurchaserStore = [[UINavigationController alloc] initWithRootViewController:purchaserStoreViewController];
        
        MSCollectorMapViewController *collectorMapViewController = [[MSCollectorMapViewController alloc] initWithNibName:@"MSCollectorMapViewController" bundle:nil];
        UINavigationController *navRootControllerMap = [[UINavigationController alloc] initWithRootViewController:collectorMapViewController];
        
        MSPurchaserAllItemsViewController *purchaserAllItemsViewController = [[MSPurchaserAllItemsViewController alloc] initWithNibName:@"MSPurchaserAllItemsViewController" bundle:nil];
        UINavigationController *navRootPurchaserAllItems = [[UINavigationController alloc] initWithRootViewController:purchaserAllItemsViewController];
        
        SCViewController *scanViewController = [[SCViewController alloc] init];
        UINavigationController *navRootPurchaserQRScan = [[UINavigationController alloc] initWithRootViewController:scanViewController];
        
        [tabBarController setViewControllers:@[[[UIViewController alloc] init],navRootPurchaserStore,navRootControllerMap,navRootPurchaserAllItems,navRootPurchaserQRScan,[[UIViewController alloc] init]]];
        tabBarController.selectedIndex = 1;
        [self.sideMenuViewController setContentViewController:tabBarController animated:YES];
        [self.sideMenuViewController hideMenuViewController];

        
        
        
    }
}


- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(homeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"logo_museum_colored"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
   
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [searchBtn addTarget:self action:@selector(searchButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"search_bar_button"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
    [self.navigationItem setRightBarButtonItem:searchBarButton];
    
}
-(void)searchButtonTapped{
    MSSearchViewController *searchViewController = [[MSSearchViewController alloc]initWithNibName:@"MSSearchViewController" bundle:nil];
    [self.navigationController pushViewController:searchViewController animated:YES];
}

-(void)homeButtonTapped{
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
