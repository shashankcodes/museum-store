//
//  MSCollectorExhibitViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSCollectorExhibitViewController.h"
#import "MSProfileCollectionViewCell.h"
#import "MSHomeViewController.h"
#import "RESideMenu.h"
#import "MSCollectorGameAlbumDetailsViewController.h"
#import "MSSearchViewController.h"
@interface MSCollectorExhibitViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *exhibitCollectionView;

@end

@implementation MSCollectorExhibitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         self.title = @"Exhibit";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    [self customizeNavigationBar];
    [self.exhibitCollectionView registerNib:[UINib nibWithNibName:@"MSProfileCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MSProfileCollectionViewCell"];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(homeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"logo_museum_colored"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [searchBtn addTarget:self action:@selector(searchButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"search_bar_button"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
    [self.navigationItem setRightBarButtonItem:searchBarButton];
    
}
-(void)searchButtonTapped{
    MSSearchViewController *searchViewController = [[MSSearchViewController alloc]initWithNibName:@"MSSearchViewController" bundle:nil];
    [self.navigationController pushViewController:searchViewController animated:YES];
}
-(void)homeButtonTapped{
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
}
#pragma mark - Collection view data source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 80;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MSProfileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MSProfileCollectionViewCell" forIndexPath:indexPath];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MSCollectorGameAlbumDetailsViewController *collectorGameAlbumDetailsViewController = [[MSCollectorGameAlbumDetailsViewController alloc]initWithNibName:@"MSCollectorGameAlbumDetailsViewController" bundle:nil];
    [self.navigationController pushViewController:collectorGameAlbumDetailsViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
