//
//  MSCollectorGameAlbumDetailsViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSCollectorGameAlbumDetailsViewController.h"
#import "MSCollectorAlbumViewCollectioViewCell.h"
#import "MSBeaconDetector.h"
#import "MSTabbarViewController.h"

@interface MSCollectorGameAlbumDetailsViewController (){
    NSMutableDictionary *dict;
}
@property (weak, nonatomic) IBOutlet UICollectionView *albumCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UITextView *secondTextView;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UITextView *artHistoryTextView;

@end

@implementation MSCollectorGameAlbumDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Album Name";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    dict = [[NSMutableDictionary alloc]initWithDictionary:@{@"itemName":@"Scandinavian Sculpture",
                                                            @"name":@"Emily Vancover",
                                                            
                                                            @"desc":@"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.",
                                                            
                                                            @"artHistory":@"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                                                            
                                                            @"cellItemName":@"Related Item #1",
                                                            @"price":@"$45",
                                                            }];
    self.itemNameLabel.text = [dict objectForKey:@"itemName"];
    self.nameLabel.text = [dict objectForKey:@"name"];
    self.descriptionTextView.text = [dict objectForKey:@"desc"];
    self.artHistoryTextView.text = [dict objectForKey:@"artHistory"];
    [self customizeNavigationBar];
    [self.albumCollectionView registerNib:[UINib nibWithNibName:@"MSCollectorAlbumViewCollectioViewCell" bundle:nil] forCellWithReuseIdentifier:@"MSCollectorAlbumViewCollectioViewCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beaconAdded:) name:@"beaconNotification" object:nil];
    [self beaconAdded:nil];
    
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)openFullScreenImageButtonTapped:(id)sender {
    self.topView.hidden = NO;
    [self.navigationController setNavigationBarHidden:YES];
    [(MSTabbarViewController*)self.tabBarController setTabBarHidden:YES];
}
- (IBAction)xButtonTapped:(id)sender {
    self.topView.hidden = YES;
    [self.navigationController setNavigationBarHidden:NO];
    [(MSTabbarViewController*)self.tabBarController setTabBarHidden:NO];
}

-(void)beaconAdded:(NSNotification *)Notification
{
    int  beaconCount = [MSBeaconDetector SharedInstance].currentUser.beacons.count;
    self.scoreLabel.text = [@(beaconCount*20) stringValue];
}
#pragma mark - Collection view data source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 20;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MSCollectorAlbumViewCollectioViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MSCollectorAlbumViewCollectioViewCell" forIndexPath:indexPath];
    cell.itemNameLabel.text = [dict objectForKey:@"cellItemName"];
    cell.priceLabel.text = [dict objectForKey:@"price"];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MSCollectorGameAlbumDetailsViewController *collectorGameAlbumDetailsViewController = [[MSCollectorGameAlbumDetailsViewController alloc]initWithNibName:@"MSCollectorGameAlbumDetailsViewController" bundle:nil];
    [self.navigationController pushViewController:collectorGameAlbumDetailsViewController animated:YES];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(backButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"back_bar_button"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
    UIButton *exportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [exportBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [exportBtn addTarget:self action:@selector(exportButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [exportBtn setImage:[UIImage imageNamed:@"search_bar_button"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *exportBarButton = [[UIBarButtonItem alloc] initWithCustomView:exportBtn];
    [self.navigationItem setRightBarButtonItem:exportBarButton];
    
}
- (IBAction)moreButtonTapped:(id)sender {
    self.secondTextView.scrollEnabled  = YES;
    [self.moreButton setHidden:YES];
    self.secondTextView.text = [NSString stringWithFormat:@"%@ %@",self.secondTextView.text,self.secondTextView.text];
}
-(void)exportButtonTapped{
    
}
-(void)backButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
