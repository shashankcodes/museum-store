//
//  MSCollectorGameViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//
#import "MSHomeViewController.h"
#import "MSCollectorGameViewController.h"
#import "MSCollectorGameTableViewCell.h"
#import "MSCollectorGameAlbumDetailsViewController.h"
#import "RESideMenu.h"
#import "MSBeaconDetector.h"
#import "MSSearchViewController.h"

static NSString *collectorGameTableViewCellIdentifier = @"collectorGameTableViewCellIdentifier";
@interface MSCollectorGameViewController (){
    NSMutableDictionary* dict;
}
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *albumsTableView;
@property (weak, nonatomic) IBOutlet UIImageView *animatedView;
@property (weak, nonatomic) IBOutlet UILabel *beaconsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *ipadPreviewCount;
@property (weak, nonatomic) IBOutlet UIButton *newlyFoundBeaconButton;
@property (weak, nonatomic) IBOutlet UILabel *findBeaconsLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
@property (strong, nonatomic) NSTimer *timer ;
@end

@implementation MSCollectorGameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dict = [[NSMutableDictionary alloc]initWithDictionary:@{@"findBeacons":@"FIND BEACONS IN THE ART GALLERY TO EXPLORE THE PIECE",
                                                            @"about":@"Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. ",
                                                            @"artName":@"Art Name Lorem Ipsum",
                                                            @"desc":@"Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. "
                                                            
                                                            }];
   
    self.findBeaconsLabel.text = [dict objectForKey:@"findBeacons"];
    self.aboutLabel.text = [dict objectForKey:@"about"];
    // Do any additional setup after loading the view from its nib.
    self.title = @"MUSEUM NAME";
    [self customizeNavigationBar];
    
    [self.albumsTableView setTableHeaderView:self.headerView];
    [self.albumsTableView registerNib:[UINib nibWithNibName:@"MSCollectorGameTableViewCell" bundle:nil] forCellReuseIdentifier:collectorGameTableViewCellIdentifier];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beaconAdded:) name:@"beaconNotification" object:nil];
    [self beaconAdded:nil];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0/25
                                              target:self
                                            selector:@selector(rotate)
                                            userInfo:nil
                                             repeats:YES];
    [_timer fire];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_timer invalidate];
    _timer = nil;
}

-(void)beaconAdded:(NSNotification *)Notification
{
    NSInteger  beaconCount = [MSBeaconDetector SharedInstance].currentUser.beacons.count;
    if (beaconCount>0) {
        self.newlyFoundBeaconButton.hidden = NO;
    }
    self.beaconsCountLabel.text = [NSString stringWithFormat:@"%ld/%d",(long)beaconCount,16];
    self.ipadPreviewCount.text = [@(beaconCount) stringValue];
    [self.albumsTableView reloadData];

}
- (void)rotate {
    static int rotation = 0;
    
    // assuming one whole rotation per second
    rotation += 360.0 / 25.0;
    if (rotation > 360.0) {
        rotation -= 360.0;
    }
    self.animatedView.transform = CGAffineTransformMakeRotation(rotation * M_PI / 180.0);
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
        UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
        [menuBtn addTarget:self action:@selector(homeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [menuBtn setImage:[UIImage imageNamed:@"logo_museum_colored"] forState:UIControlStateNormal];
        //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
        UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
        [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [searchBtn addTarget:self action:@selector(searchButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"search_bar_button"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
    [self.navigationItem setRightBarButtonItem:searchBarButton];
    
}
-(void)searchButtonTapped{
    MSSearchViewController *searchViewController = [[MSSearchViewController alloc]initWithNibName:@"MSSearchViewController" bundle:nil];
    [self.navigationController pushViewController:searchViewController animated:YES];
}
-(void)homeButtonTapped{
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (IBAction)viewNewFoundBeaconButtonTapped:(id)sender {
    MSCollectorGameAlbumDetailsViewController *collectorGameAlbumDetailsViewController = [[MSCollectorGameAlbumDetailsViewController alloc]initWithNibName:@"MSCollectorGameAlbumDetailsViewController" bundle:nil];
    [self.navigationController pushViewController:collectorGameAlbumDetailsViewController animated:YES];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MSCollectorGameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:collectorGameTableViewCellIdentifier];
    NSArray *detectedArray = [MSBeaconDetector SharedInstance].detectedBeaconIndexesArray;
    cell.artNameLabel.text = [dict objectForKey:@"artName"];
    cell.descLabel.text = [dict objectForKey:@"desc"];
    if ([detectedArray containsObject:@(indexPath.row)])
    {
        cell.tickImageView.hidden = NO;
    }
    else
    {
        cell.tickImageView.hidden = YES;
    }
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MSCollectorGameAlbumDetailsViewController *collectorGameAlbumDetailsViewController = [[MSCollectorGameAlbumDetailsViewController alloc]initWithNibName:@"MSCollectorGameAlbumDetailsViewController" bundle:nil];
    [self.navigationController pushViewController:collectorGameAlbumDetailsViewController animated:YES];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 16;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
