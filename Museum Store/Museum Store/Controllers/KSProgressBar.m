//
//  KSProgressBar
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "KSProgressBar.h"

@interface KSProgressBar ()

@property (nonatomic, strong) UIImage *progressImage;
@property (nonatomic, strong) UIImage *trackImage;
@property (nonatomic, weak) UIImageView *trackImageView;
@property (nonatomic, weak) UIImageView *progressImageView;

@end
@implementation KSProgressBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.progressImage=[[UIImage imageNamed:@"progress_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
        self.trackImage=[[UIImage imageNamed:@"progress_blank"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
        [self setupTrackImageView];
        [self setupProgressImageView];
    }
    return self;
}

-(void)setupTrackImageView
{
    UIImageView *trackImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [trackImageView setImage:[self.trackImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)]];
    [trackImageView.layer setMasksToBounds:YES];
    [self addSubview:trackImageView];
    self.trackImageView = trackImageView;
}

-(void)setupProgressImageView
{
    UIImageView *progressImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [progressImageView setImage:[self.progressImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)]];
    progressImageView.layer.masksToBounds = YES;
    [self.trackImageView addSubview:progressImageView];
    self.progressImageView = progressImageView;
}

-(void)setProgressValue:(CGFloat)progressValue
{
    if (progressValue > 0) {
        [self setUnityProgress:progressValue];
        [UIView animateWithDuration:0.1f animations:^{
            [self.progressImageView setFrame:CGRectMake(0, 0, _progressValue * CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds))];
        }];
    }
}

- (void)setUnityProgress:(float)progress
{
    if(isnan(progress))
    {
        progress = 0;
    }
    
    if(progress >= 1)
    {
        progress = 1;
    }
    
    _progressValue = progress;
}

@end
