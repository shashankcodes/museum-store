//
//  MSPurchaserTabbarController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSPurchaserTabbarController.h"

#define kCustomTabBarTag 4612
#define kTabBarItemLabelTag 389
#define widthTabBar 768

@interface MSPurchaserTabbarController ()

@end

@implementation MSPurchaserTabbarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupCustomTabBar];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)setupCustomTabBar
{
	//custom tab bar view
	UIView *customTabBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, widthTabBar, 56.0)];
	customTabBarView.tag = 1010;
	//set custom tab bar bg image
	UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tab_bar_background.png"]];
    
	bgImageView.frame = CGRectMake(0.0, 0.0, widthTabBar, 56.0);
	[customTabBarView addSubview:bgImageView];
	
	//add listen button to custom tab bar
    
    
    UIButton *firstButton = [UIButton buttonWithType:UIButtonTypeCustom];
	firstButton.frame = CGRectMake(0.0, 0.0, 175.0, 53.0);
	firstButton.tag = 10;
    firstButton.backgroundColor = [UIColor clearColor];
    [self setButton:firstButton selected:NO];
	[customTabBarView addSubview:firstButton];
	
    
    UIButton *secondButton = [UIButton buttonWithType:UIButtonTypeCustom];
	secondButton.frame = CGRectMake(175, 0, 105.0, 53.0);
	secondButton.tag = 11;
    [secondButton setImage:[UIImage imageNamed:@"store_selected.png"] forState:UIControlStateSelected];
	[secondButton setImage:[UIImage imageNamed:@"store_selected.png"] forState:UIControlStateHighlighted];
	[secondButton setImage:[UIImage imageNamed:@"store_tab.png"] forState:UIControlStateNormal];
    
    secondButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 38.0, 18.0, 0.0);
    [secondButton setTitleEdgeInsets:UIEdgeInsetsMake(35.0, -3, 0, 18)];
    [secondButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    
    [self setButton:secondButton selected:YES];

    // [gameButton setBackgroundColor:[UIColor redColor]];
    [secondButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
	[customTabBarView addSubview:secondButton];
    [secondButton setTitle:@"STORE" forState:UIControlStateNormal];
    
    UIButton *thirdButton = [UIButton buttonWithType:UIButtonTypeCustom];
	thirdButton.frame = CGRectMake(280, 0, 105.0,53);
	thirdButton.tag = 12;
	[thirdButton setImage:[UIImage imageNamed:@"map_icon_selected.png"] forState:UIControlStateSelected];
	[thirdButton setImage:[UIImage imageNamed:@"map_icon_selected.png"] forState:UIControlStateHighlighted];
	[thirdButton setImage:[UIImage imageNamed:@"map_tab.png"] forState:UIControlStateNormal];
    thirdButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 33.0, 18.0, 0.0);
    [thirdButton setTitleEdgeInsets:UIEdgeInsetsMake(35.0, 5, 0, 15)];
    [thirdButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [thirdButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
    [thirdButton setTitle:@"MAP" forState:UIControlStateNormal];
    [customTabBarView addSubview:thirdButton];
    
	//add me button to custom tab bar
	UIButton *fourthButton = [UIButton buttonWithType:UIButtonTypeCustom];
	fourthButton.frame = CGRectMake(385.0, 0.0, 106.0, 53.0);
    fourthButton.tag = 13;
    [fourthButton setImage:[UIImage imageNamed:@"all_items_selected.png"] forState:UIControlStateSelected];
	[fourthButton setImage:[UIImage imageNamed:@"all_items_selected.png"] forState:UIControlStateHighlighted];
	[fourthButton setImage:[UIImage imageNamed:@"exhibit_tab.png"] forState:UIControlStateNormal];
    
    [fourthButton setTitle:@"ALL ITEMS" forState:UIControlStateNormal];
    fourthButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 38.0, 18.0, 0.0);
    [fourthButton setTitleEdgeInsets:UIEdgeInsetsMake(35.0, 0, 0, 15)];
    [fourthButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
	[customTabBarView addSubview:fourthButton];
    [fourthButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
	//add stats button to custom tab bar
	UIButton *fifthButton = [UIButton buttonWithType:UIButtonTypeCustom];	fifthButton.frame = CGRectMake(487.0, 0.0, 106.0, 53.0);
    fifthButton.tag = 14;
    [fifthButton setImage:[UIImage imageNamed:@"qr_scan_selected.png"] forState:UIControlStateSelected];
	[fifthButton setImage:[UIImage imageNamed:@"qr_scan_selected.png"] forState:UIControlStateHighlighted];
	[fifthButton setImage:[UIImage imageNamed:@"qr_scan.png"] forState:UIControlStateNormal];
    [fifthButton setTitle:@"QR SCAN" forState:UIControlStateNormal];
    fifthButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 38.0, 18.0, 0.0);
    [fifthButton setTitleEdgeInsets:UIEdgeInsetsMake(35.0, -3, 0, 18)];
    [fifthButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
	[customTabBarView addSubview:fifthButton];
    [fifthButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *sixthButton = [UIButton buttonWithType:UIButtonTypeCustom];
	sixthButton.frame = CGRectMake(593, 0, 175.0,53);
	sixthButton.tag = 15;
    [sixthButton setBackgroundColor:[UIColor clearColor]];
    [customTabBarView addSubview:sixthButton];

    
	
	MSPurchaserTabbarController *tabBarController = self;
	UIView *tabBarControllerView = [tabBarController view];
	
	customTabBarView.frame = CGRectMake(0, tabBarControllerView.frame.size.height - 56, widthTabBar, 56.0);
	customTabBarView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
	
	[tabBarControllerView addSubview:customTabBarView];
	
	[self.tabBar setHidden:YES];
}

- (void)setButton:(UIButton *)button selected:(BOOL)selected
{
	[button setSelected:selected];
	[button setUserInteractionEnabled:!selected];
	
	
	if (selected)
	{
		
	}
	else
	{
		
	}
}

- (void)customTabBarAction:(UIButton *)sender
{
    
    
    [self setSelectedIndex:sender.tag - 10];
    
    
    [self setButton:sender selected:YES];
    
	
	for (int i = 10; i < 15; i++)
	{
		if (i != sender.tag)
		{
			UIView *buttonView = [self.view viewWithTag:i];
			if ([buttonView isKindOfClass:[UIButton class]])
			{
				[self setButton:(UIButton *)buttonView selected:NO];
			}
		}
		//if we switch to me tab fetch current user details
	}
	
}





- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated
{
	UIView *customTabBar = [self.view viewWithTag:kCustomTabBarTag];
	
    if (hidden == customTabBar.hidden)
        return;
	
	if (animated)
	{
		customTabBar.hidden = NO;
        
		[UIView animateWithDuration:(animated?.3:.0) animations:^(){
			
            CGRect frame = customTabBar.frame;
			frame.origin.y = (hidden?self.view.bounds.size.height:(self.view.bounds.size.height-frame.size.height));
            customTabBar.frame = frame;
            self.tabBar.hidden = hidden;
            
		}completion:^(BOOL finished){
			
            customTabBar.hidden = hidden;
            self.tabBar.hidden = hidden;
		}];
	}
	else
	{
		customTabBar.hidden = hidden;
        self.tabBar.hidden = hidden;
	}
    
}
- (void)setTabBarHidden:(BOOL)hidden
{
//    [self setTabBarHidden:hidden animated:NO];
    
    [self.tabBar setHidden:hidden];
    UIView *buttonView = [self.view viewWithTag:1010];
    [buttonView setHidden:hidden];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
