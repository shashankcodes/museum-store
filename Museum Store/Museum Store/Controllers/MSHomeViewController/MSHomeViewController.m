//
//  MSHomeViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSHomeViewController.h"
#import "RESideMenu.h"
#import "MSTabbarViewController.h"
#import "MSCollectorGameViewController.h"
#import "MSCollectorExhibitViewController.h"
#import "MSCollectorMapViewController.h"
#import "SCViewController.h"
#import "MSPurchaserTabbarController.h"
#import "MSPurchaserAllItemsViewController.h"
#import "MSPurchaserStoreViewController.h"
#import "MSPurchaserMapViewController.h"
#import "MSProfileViewController.h"
#import "MSAboutTheMuseumViewController.h"
@interface MSHomeViewController (){
    NSMutableDictionary* dict;
}
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *reverserArrowButton;
@property (weak, nonatomic) IBOutlet UILabel *learnMoreAboutMuseumLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutTheMuseumLabel;
@property (weak, nonatomic) IBOutlet UILabel *exploreCollectorLabel;
@property (weak, nonatomic) IBOutlet UILabel *exploreStoreLabel;

@end

@implementation MSHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         self.title = @"LOREM IPSUM MUSEUM";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view from its nib.
   
    dict = [[NSMutableDictionary alloc]initWithDictionary:@{@"learnMoreAboutMuseum":@"Welcome To Lorem Ipsum Museum",
                                                            @"aboutTheMuseum":@"More about the Museum Comes Here",
                                                            @"exploreCollector":@"Explore the gamification area, and trade every point with discount from our store.",
                                                            @"exploreStore":@"Explore our Store, find gift or item about museum and much more."
                                                            
                                                            }];
    
    self.learnMoreAboutMuseumLabel.text = [dict objectForKey:@"learnMoreAboutMuseum"];
    self.aboutTheMuseumLabel.text = [dict objectForKey:@"aboutTheMuseum"];
    self.exploreCollectorLabel.text = [dict objectForKey:@"exploreCollector"];
    self.exploreStoreLabel.text = [dict objectForKey:@"exploreStore"];
    [self customizeNavigationBar];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(presentLeftMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
//    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [backBtn setFrame:CGRectMake(0.0f, 0.0f, 45.0f, 20.0f)];
//    [backBtn addTarget:self action:@selector(backButtonTapped) forControlEvents:UIControlEventTouchUpInside];
//    [backBtn setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
//    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    
//    [self.navigationItem setRightBarButtonItem:backBarButton];
}
- (IBAction)aboutTheMuseumTapped:(id)sender {
    MSAboutTheMuseumViewController *aboutTheMuseumViewController = [[MSAboutTheMuseumViewController alloc]initWithNibName:@"MSAboutTheMuseumViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:aboutTheMuseumViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)collectorButtonTapped:(id)sender
{
//    MSCollectorViewController *collectorController = [[MSCollectorViewController alloc] initWithNibName:@"MSCollectorViewController" bundle:nil];
    MSTabbarViewController *tabBarController = [[MSTabbarViewController alloc] initWithNibName:@"MSTabbarViewController" bundle:nil];
    
    MSCollectorGameViewController *collectorGameViewController = [[MSCollectorGameViewController alloc] initWithNibName:@"MSCollectorGameViewController" bundle:nil];
    UINavigationController *navRootControllerGame = [[UINavigationController alloc] initWithRootViewController:collectorGameViewController];
    
    MSCollectorExhibitViewController *collectorExhibitViewController = [[MSCollectorExhibitViewController alloc] initWithNibName:@"MSCollectorExhibitViewController" bundle:nil];
    UINavigationController *navRootControllerExhibit = [[UINavigationController alloc] initWithRootViewController:collectorExhibitViewController];
    MSCollectorMapViewController *collectorMapViewController = [[MSCollectorMapViewController alloc] initWithNibName:@"MSCollectorMapViewController" bundle:nil];
    UINavigationController *navRootControllerMap = [[UINavigationController alloc] initWithRootViewController:collectorMapViewController];
    
    MSProfileViewController *profileViewController = [[MSProfileViewController alloc] initWithNibName:@"MSProfileViewController" bundle:nil];
    UINavigationController *navRootControllerProfile = [[UINavigationController alloc] initWithRootViewController:profileViewController];
    
    UIViewController *viewController = [[UIViewController alloc] init];
    
    [tabBarController setViewControllers:@[navRootControllerProfile,navRootControllerGame,navRootControllerExhibit,navRootControllerMap,[[UIViewController alloc] init],[[UIViewController alloc] init]]];
    tabBarController.selectedIndex = 1;
    [self.sideMenuViewController setContentViewController:tabBarController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
    
}
- (IBAction)arrowButtonTapped:(id)sender {
    [self.reverserArrowButton setHidden:NO];
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomView.frame = CGRectMake(0, 611, self.bottomView.frame.size.width
                                           , self.bottomView.frame.size.height);
    }];
}
- (IBAction)reverseArrowButtonTapped:(id)sender {
    [self.reverserArrowButton setHidden:YES];
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomView.frame = CGRectMake(0, 511, self.bottomView.frame.size.width
                                           , self.bottomView.frame.size.height);
    }];
}
- (IBAction)purchaserButtonTapped:(id)sender
{
    
    MSPurchaserTabbarController *tabBarController = [[MSPurchaserTabbarController alloc] init];

    
    MSPurchaserStoreViewController *purchaserStoreViewController = [[MSPurchaserStoreViewController alloc] initWithNibName:@"MSPurchaserStoreViewController" bundle:nil];
    UINavigationController *navRootPurchaserStore = [[UINavigationController alloc] initWithRootViewController:purchaserStoreViewController];
    
    MSCollectorMapViewController *collectorMapViewController = [[MSCollectorMapViewController alloc] initWithNibName:@"MSCollectorMapViewController" bundle:nil];
    UINavigationController *navRootControllerMap = [[UINavigationController alloc] initWithRootViewController:collectorMapViewController];
    
    MSPurchaserAllItemsViewController *purchaserAllItemsViewController = [[MSPurchaserAllItemsViewController alloc] initWithNibName:@"MSPurchaserAllItemsViewController" bundle:nil];
    UINavigationController *navRootPurchaserAllItems = [[UINavigationController alloc] initWithRootViewController:purchaserAllItemsViewController];
    
    SCViewController *scanViewController = [[SCViewController alloc] init];
    UINavigationController *navRootPurchaserQRScan = [[UINavigationController alloc] initWithRootViewController:scanViewController];
    
    [tabBarController setViewControllers:@[[[UIViewController alloc] init],navRootPurchaserStore,navRootControllerMap,navRootPurchaserAllItems,navRootPurchaserQRScan,[[UIViewController alloc] init]]];
    tabBarController.selectedIndex = 1;
    [self.sideMenuViewController setContentViewController:tabBarController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
