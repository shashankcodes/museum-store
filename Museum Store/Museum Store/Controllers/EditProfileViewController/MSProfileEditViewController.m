//
//  ProfileEditViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.//

#import "MSProfileEditViewController.h"

@interface MSProfileEditViewController ()


@end

@implementation MSProfileEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelTapped:(id)sender
{
    if (self.cancelBlock)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:self.firstNameField.text forKey:@"first_name"];
        [dict setObject:self.lastNameField.text forKey:@"last_name"];
        [dict setObject:self.locationField.text forKey:@"location"];
        [dict setObject:self.collectorField.text forKey:@"collector"];
        self.cancelBlock(dict);
    }
}

@end
