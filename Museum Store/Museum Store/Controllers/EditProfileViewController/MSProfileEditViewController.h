//
//  ProfileEditViewController.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved..
//

#import <UIKit/UIKit.h>

typedef void(^CancelBlock)(id obj);


@interface MSProfileEditViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *locationField;
@property (weak, nonatomic) IBOutlet UITextField *collectorField;
@property (copy, nonatomic) CancelBlock cancelBlock;

@end
