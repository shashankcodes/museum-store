//
//  LeftMenuViewController.m
//  RESideMenuExample
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "MSRootViewController.h"
#import "MSBeaconDetector.h"
#import "MSHomeViewController.h"
#import "MSSearchViewController.h"
#import "MSPurchaserStoreViewController.h"
#import "MSAboutTheMuseumViewController.h"
#import "MSProfileViewController.h"
@interface LeftMenuViewController ()

@end

@implementation LeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height - 54 * 6) / 2.0f, self.view.frame.size.width, 54 * 6) style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        [tableView setBackgroundColor:[UIColor clearColor]];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        tableView.bounces = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    [self.view bringSubviewToFront:self.tableView];
}
-(void)viewDidAppear:(BOOL)animated{
    [self.tableView reloadData];
}
#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:{
            MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
            UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
            [self.sideMenuViewController setContentViewController:navRootController animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;
        case 1:{
            MSPurchaserStoreViewController *purchaserStoreViewController = [[MSPurchaserStoreViewController alloc]initWithNibName:@"MSPurchaserStoreViewController" bundle:nil];
            UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:purchaserStoreViewController];
            [self.sideMenuViewController setContentViewController:navRootController animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;
        case 2:{
            MSSearchViewController *searchViewController = [[MSSearchViewController alloc]initWithNibName:@"MSSearchViewController" bundle:nil];
            UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:searchViewController];
            [self.sideMenuViewController setContentViewController:navRootController animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;
        case 3:{
            MSAboutTheMuseumViewController *aboutTheMuseumViewController = [[MSAboutTheMuseumViewController alloc]initWithNibName:@"MSAboutTheMuseumViewController" bundle:nil];
            UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:aboutTheMuseumViewController];
            [self.sideMenuViewController setContentViewController:navRootController animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;
        case 4:{
            MSProfileViewController *profileViewController = [[MSProfileViewController alloc]initWithNibName:@"MSProfileViewController" bundle:nil];
            UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:profileViewController];
            [self.sideMenuViewController setContentViewController:navRootController animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;
        case 5:{
            [self resetDefaults];
            [MSBeaconDetector SharedInstance].currentUser= nil;
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[MSRootViewController alloc] init]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
        }
                default:
            break;
    }
}
- (void)resetDefaults {
    
    [[MSBeaconDetector SharedInstance].basketArray removeAllObjects];
    [MSBeaconDetector SharedInstance].detectedBeaconIndexesArray = nil;
    
    
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"foundMuseum"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"foundStore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}
#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:21];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(1, 46, 254, 1)];/// change size as you need.
        separatorLineView.backgroundColor = [UIColor colorWithRed:38.0/255.0 green:36.0/255.0 blue:32.0/255.0 alpha:1];// you can also put image here
        [cell.contentView addSubview:separatorLineView];
    }
    
    NSArray *titles = @[@"Home", @"Store", @"Advanced Search", @"About the Museum",@"My Preferences", @"Log Out"];
    NSArray *images = @[@"home_icon_sidebar", @"store_icon_sidebar", @"advanced_search_icon_sidebar", @"about_the_museum_icon_sidebar",@"my_prefrences_icon_sidebar", @"logout_icon_sidebar"];
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}

@end
