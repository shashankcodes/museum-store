//
//  MSRootViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSRootViewController.h"
#import "MSLoginManager.h"
#import "MSHomeViewController.h"
#import "RESideMenu.h"
#import "MSUser.h"
#import "MSAppDelegate.h"
#import "MSBeaconDetector.h"
#import "SVProgressHUD.h"

@interface MSRootViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;

@property (strong,nonatomic) MSLoginManager *loginManager;

@end

@implementation MSRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.loginManager = [[MSLoginManager alloc] init];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)facebookLoginTapped:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Signing In" maskType:SVProgressHUDMaskTypeClear];
    
    
    [self.loginManager fbLoginWithsuccessBlock:^(id response)
    {
        [SVProgressHUD dismiss];
        
        MSAppDelegate *appDel = (MSAppDelegate *)[UIApplication sharedApplication].delegate ;
        NSArray *allUsersArray = [MSUser getAllInMOC:appDel.managedObjectContext];
        
        [[NSUserDefaults standardUserDefaults] setObject:response[kEmailAddress] forKey:kEmailAddress];
        if (response[kFirstName])
        {
              [[NSUserDefaults standardUserDefaults] setObject:response[kFirstName] forKey:kFirstName];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kFirstName] ;
        }
        
        if (response[kLastName])
        {
            [[NSUserDefaults standardUserDefaults] setObject:response[kLastName] forKey:kLastName];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kLastName] ;
        }
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username == %@",response[kEmailAddress]];
        NSArray *filteredArray = [allUsersArray filteredArrayUsingPredicate:predicate];
        MSBeaconDetector *beaconDet = [MSBeaconDetector SharedInstance];
        if(!filteredArray.count)
        {
            beaconDet.currentUser = [NSEntityDescription insertNewObjectForEntityForName:@"MSUser" inManagedObjectContext:appDel.managedObjectContext];
            beaconDet.currentUser.username = response[kEmailAddress];

            
            
            NSError *error;
            [appDel.managedObjectContext save:&error];
            
        }
        else
        {
            beaconDet.currentUser = filteredArray[0];
        }

        [self loginSuccess];
    } FailureBlock:^(NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            NSString* errorMessage = [error.userInfo objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Museum Store"
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            
            [alert show];
        });
        
       
    }];
}
- (IBAction)twitterLoginTapped:(id)sender
{
    
    [self.loginManager twitterLoginWithsuccessBlock:^(id response)
    {
        [SVProgressHUD dismiss];
        MSAppDelegate *appDel = (MSAppDelegate *)[UIApplication sharedApplication].delegate ;
        NSArray *allUsersArray = [MSUser getAllInMOC:appDel.managedObjectContext];
        
        [[NSUserDefaults standardUserDefaults] setObject:response[kEmailAddress] forKey:kEmailAddress];
        if (response[kFirstName])
        {
            [[NSUserDefaults standardUserDefaults] setObject:response[kFirstName] forKey:kFirstName];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kFirstName] ;
        }
        
        if (response[kLastName])
        {
            [[NSUserDefaults standardUserDefaults] setObject:response[kLastName] forKey:kLastName];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kLastName] ;
        }

        
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username == %@",response[kEmailAddress]];
        NSArray *filteredArray = [allUsersArray filteredArrayUsingPredicate:predicate];
        MSBeaconDetector *beaconDet = [MSBeaconDetector SharedInstance];
        if(!filteredArray.count)
        {
            beaconDet.currentUser = [NSEntityDescription insertNewObjectForEntityForName:@"MSUser" inManagedObjectContext:appDel.managedObjectContext];
            NSError *error;
            beaconDet.currentUser.username = response[kEmailAddress];

            [appDel.managedObjectContext save:&error];
            
        }
        else
        {
            beaconDet.currentUser = filteredArray[0];
        }
        
        [self loginSuccess];

    } FailureBlock:^(NSError *error) {
        
          dispatch_async(dispatch_get_main_queue(), ^{
              [SVProgressHUD dismiss];
        NSString* errorMessage = [error.userInfo objectForKey:@"message"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Museum Store"
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
          });
    }];
}
- (IBAction)guestLoginData:(id)sender
{
    
    [self flipFromView1:self.view1 toView:self.view2 rTol:NO duration:0.3 completion:nil];
}
- (IBAction)loginButtonTapped:(id)sender
{
    NSString *username = self.userNameField.text;
    NSString *userID = self.passwordField.text;
    
    if (username.length && userID.length)
    {
    
        
        
    MSAppDelegate *appDel = (MSAppDelegate *)[UIApplication sharedApplication].delegate ;
   NSArray *allUsersArray = [MSUser getAllInMOC:appDel.managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username == %@",username];
   NSArray *filteredArray = [allUsersArray filteredArrayUsingPredicate:predicate];
    MSBeaconDetector *beaconDet = [MSBeaconDetector SharedInstance];
    if(!filteredArray.count)
    {
         beaconDet.currentUser = [NSEntityDescription insertNewObjectForEntityForName:@"MSUser" inManagedObjectContext:appDel.managedObjectContext];
        beaconDet.currentUser.username = username;
        beaconDet.currentUser.userId = userID;
        
        NSError *error;
        [appDel.managedObjectContext save:&error];
        
        [[NSUserDefaults standardUserDefaults] setObject:username forKey:kFirstName];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kEmailAddress];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kLastName];
        
        [self loginSuccess];
    }
    else
    {
        MSUser *user = filteredArray[0];
       if([user.userId isEqualToString:userID])
       {
           beaconDet.currentUser = user;
           
           [[NSUserDefaults standardUserDefaults] setObject:username forKey:kFirstName];
           [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kEmailAddress];
           [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kLastName];

           [self loginSuccess];
           
       }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Museum Store" message:@"Authentication Failed. Try Again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        
    }
    }
    else
    {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Museum Store" message:@"Please enter both username and password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
    
}

-(void) loginSuccess
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc] initWithNibName:@"MSHomeViewController" bundle:nil];
    
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
    [self.sideMenuViewController hideMenuViewController];

}

- (IBAction)cancelButtonTapped:(id)sender {
    [self flipFromView1:self.view2 toView:self.view1 rTol:YES duration:0.3 completion:nil];
}
-(void)flipFromView1:(UIView*)v1 toView:(UIView*)v2 rTol:(BOOL)rTol duration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion
{
    duration = duration/2;
    
    [v2.layer setAffineTransform:CGAffineTransformMakeScale(0,1)];
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [v1.layer setAffineTransform:CGAffineTransformMakeScale(0,1)];
    } completion:^(BOOL finished){
        [v1 setHidden:YES];
        [v2 setHidden:NO];
    }];
    
    [UIView animateWithDuration:duration delay:duration options:UIViewAnimationOptionCurveEaseOut animations:^{
        [v2.layer setAffineTransform:CGAffineTransformMakeScale(1,  1)];
    } completion:completion];
}
-(void)sucesslogin{
    
}
@end
