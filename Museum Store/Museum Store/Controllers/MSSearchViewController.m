//
//  MSSearchViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSSearchViewController.h"
#import "MSHomeViewController.h"
#import "RESideMenu.h"
@interface MSSearchViewController ()

@end

@implementation MSSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"SEARCH";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self customizeNavigationBar];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(homeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"logo_museum_colored"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];

    
}
-(void)homeButtonTapped{
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
