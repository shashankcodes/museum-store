//
//  MSPurchaserStoreCollectionViewCell.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.//

#import <UIKit/UIKit.h>
typedef void(^CollectionCellBlock)(id cell);


@interface MSPurchaserStoreCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *lockedImageView;
@property (weak, nonatomic) IBOutlet UIView *addToCartImageView;
@property (weak, nonatomic) IBOutlet UIButton *AddToCartButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (copy, nonatomic) CollectionCellBlock cellBlock;

@end
