//
//  MSPurchaserItemDetailsViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//
#import "MSPurchaserItemDetailsViewController.h"
#import "RESideMenu.h"
#import "MSCheckOutViewController.h"
#import "MSPurchaserTabbarController.h"
#import "MSBeaconDetector.h"
#import "MSSearchViewController.h"
@interface MSPurchaserItemDetailsViewController (){
    NSMutableDictionary *dict;
    UIButton *badgeBtn;
}
@property (strong, nonatomic) IBOutlet UIButton *addToBasketButton;
@property (weak, nonatomic) IBOutlet UILabel *albumName;
@property (weak, nonatomic) IBOutlet UILabel *artType;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation MSPurchaserItemDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"ITEM NAME";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dict = [[NSMutableDictionary alloc]initWithDictionary:@{@"albumName":@"Album Teresa",
                                                            @"artType":@"Modern Art",
                                                            @"descTextView":@"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda. Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.",
                                                            @"priceLabel":@"$1275,25"
                                                            
                                                            }];
    
    self.albumName.text = [dict objectForKey:@"albumName"];
    self.artType.text = [dict objectForKey:@"artType"];
    self.descTextView.text = [dict objectForKey:@"descTextView"];
    self.priceLabel.text = [dict objectForKey:@"priceLabel"];
    
    // Do any additional setup after loading the view from its nib.
    [self customizeNavigationBar];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    
    [self.tabBarController.view addSubview:self.addToBasketButton];
    self.addToBasketButton.frame = CGRectMake(556, self.tabBarController.view.frame.size.height - 56, 212, 56);
}
-(void)backButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)searchButtonTapped{
    MSSearchViewController *searchViewController = [[MSSearchViewController alloc]initWithNibName:@"MSSearchViewController" bundle:nil];
    [self.navigationController pushViewController:searchViewController animated:YES];
}
-(void)basketButtonTapped{
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    MSCheckOutViewController *checkOutViewController = [[MSCheckOutViewController alloc] initWithNibName:@"MSCheckOutViewController" bundle:nil];
    [self.navigationController pushViewController:checkOutViewController animated:YES];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(backButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"back_bar_button"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 32.0f)];
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setFrame:CGRectMake(10.0f, 0.0f, 44.0f, 32.0f)];
    [searchBtn addTarget:self action:@selector(searchButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"search_bar_button"] forState:UIControlStateNormal];
    
    UIButton *basketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [basketBtn setFrame:CGRectMake(54.0f, 0.0f, 44.0f, 32.0f)];
    [basketBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [basketBtn setImage:[UIImage imageNamed:@"cart_bar_button"] forState:UIControlStateNormal];
    
    badgeBtn = [[UIButton alloc]initWithFrame:CGRectMake(80, 0, 18.0, 18.0f)];
    [badgeBtn setBackgroundImage:[UIImage imageNamed:@"badge_icon"] forState:UIControlStateNormal];
    [badgeBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
   [badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[MSBeaconDetector SharedInstance].basketArray count]] forState:UIControlStateNormal];
    [badgeBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [view addSubview:searchBtn];
    [view addSubview:basketBtn];
    [view addSubview:badgeBtn];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    [self.navigationItem setRightBarButtonItem:searchBarButton];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.addToBasketButton removeFromSuperview];
}
- (IBAction)addToBasketButtonTapped:(id)sender {
    [[MSBeaconDetector SharedInstance].basketArray addObject:@(1)];
    [badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[MSBeaconDetector SharedInstance].basketArray count]] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
