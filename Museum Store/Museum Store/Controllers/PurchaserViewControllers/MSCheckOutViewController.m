//
//  MSCheckOutViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSCheckOutViewController.h"
#import "MSCheckOutTableViewCell.h"
#import "MSBeaconDetector.h"
#import "MSPurchaserTabbarController.h"
#import "RESideMenu.h"
#import "MSBeaconDetector.h"
static NSString *checkOutTableViewCellIdentifier = @"checkOutTableViewCellIdentifier";

@interface MSCheckOutViewController (){
    int basketCount;
    BOOL selected;
    int selectedCount;
    NSMutableDictionary *dict;
}
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UITableView *checkoutTableView;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *redeemLabel;
@property (weak, nonatomic) IBOutlet UIImageView *redeemPointsSelectionImageView;
@property (strong, nonatomic) IBOutlet UIButton *proceedToCheckoutButton;
@property (strong, nonatomic) IBOutlet UIButton *redeemPointsButton;

@end

@implementation MSCheckOutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"SHOPPING CART";
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dict = [[NSMutableDictionary alloc]initWithDictionary:@{@"albumName":@"Chair Album",
                                                            @"desc":@"product Description",
                                                            
                                                            @"art":@"Member Art",
                                                            
                                                            @"price":@"$28.99"
                                                            
                                                            }];
    
    
    
    basketCount = 0;
    selectedCount= 0;
    selected= FALSE;
    // Do any additional setup after loading the view from its nib.
    [self customizeNavigationBar];
    basketCount = [[MSBeaconDetector SharedInstance].basketArray count];
     [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    [self.checkoutTableView registerNib:[UINib nibWithNibName:@"MSCheckOutTableViewCell" bundle:nil] forCellReuseIdentifier:checkOutTableViewCellIdentifier];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beaconAdded:) name:@"beaconNotification" object:nil];
    [self beaconAdded:nil];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    
    [self.tabBarController.view addSubview:self.proceedToCheckoutButton];
    [self.tabBarController.view addSubview:self.redeemPointsButton];
    self.proceedToCheckoutButton.frame = CGRectMake(559, self.tabBarController.view.frame.size.height - 52, 209, 53);
    self.redeemPointsButton.frame = CGRectMake(11, self.tabBarController.view.frame.size.height - 56, 266, 56);
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:NO];
    [self.proceedToCheckoutButton removeFromSuperview];
    [self.redeemPointsButton removeFromSuperview];
}
-(void)beaconAdded:(NSNotification *)Notification
{
    int  beaconCount = [MSBeaconDetector SharedInstance].currentUser.beacons.count;
    self.redeemLabel.text = [NSString stringWithFormat:@"Redeem Store Points (%@)",@(beaconCount*20)];

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MSCheckOutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:checkOutTableViewCellIdentifier];
    cell.albumName.text = [dict objectForKey:@"albumName"];
    cell.descLabel.text = [dict objectForKey:@"desc"];
    cell.artLabel.text = [dict objectForKey:@"art"];
    cell.priceLabel.text = [dict objectForKey:@"price"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MSCheckOutTableViewCell *cell =(MSCheckOutTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkImageView.image = [UIImage imageNamed:@"circle_checked.png"];
    selectedCount =selectedCount + 1;
    [self.proceedToCheckoutButton setTitle:[NSString stringWithFormat:@"Proceed To CheckOut (%d)",selectedCount] forState:UIControlStateNormal];
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    MSCheckOutTableViewCell *cell =(MSCheckOutTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkImageView.image = [UIImage imageNamed:@"circle_unchecked.png"];
    selectedCount = selectedCount-1;
    [self.proceedToCheckoutButton setTitle:[NSString stringWithFormat:@"Proceed To CheckOut (%d)",selectedCount] forState:UIControlStateNormal];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return basketCount;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

-(void)backButtonTapped{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(backButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"back_bar_button"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
}
- (IBAction)redeemPointsButtonTapped:(id)sender {
    selected = !selected;
    if(selected)
    {
        self.redeemPointsSelectionImageView.image = [UIImage imageNamed:@"circle_checked"];
        
        NSString *text1 =@"Total: ($275) $240";
        
       
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString:text1];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor lightGrayColor]
                     range:NSMakeRange(7, 6)];
        [self.totalLabel setAttributedText: text];
        
        
    }else{
        self.redeemPointsSelectionImageView.image = [UIImage imageNamed:@"circle_unchecked"];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Total: $275"];
        
        [self.totalLabel setAttributedText:attributedString];
        
    }
        
}
- (IBAction)proceedToCheckoutButtonTapped:(id)sender {
    self.redeemPointsSelectionImageView.image = [UIImage imageNamed:@"circle_unchecked"];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
