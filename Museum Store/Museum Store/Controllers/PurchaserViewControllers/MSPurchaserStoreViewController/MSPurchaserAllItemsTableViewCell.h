//
//  MSPurchaserAllItemsTableViewCell.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSPurchaserAllItemsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *albumName;
@property (weak, nonatomic) IBOutlet UILabel *albumPrice;

@end
