//
//  MSPurchaserStoreViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//
#import "MSHomeViewController.h"
#import "RESideMenu.h"
#import "MSCheckOutViewController.h"
#import "MSPurchaserTabbarController.h"
#import "MSPurchaserAllItemsViewController.h"
#import "MSPurchaserAllItemsTableViewCell.h"
#import "MSPurchaserItemDetailsViewController.h"
#import "MSBeaconDetector.h"
#import "MSSearchViewController.h"
static NSString *purchaserAllItemsTableViewCellIdentifier = @"purchaserAllItemsTableViewCellIdentifier";
@interface MSPurchaserAllItemsViewController (){
    NSMutableDictionary *dict;
}
@property (weak, nonatomic) IBOutlet UITableView *allItemsTableViewCell;

@end

@implementation MSPurchaserAllItemsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"STORE NAME";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dict = [[NSMutableDictionary alloc]initWithDictionary:@{@"albumName":@"Chair Album",
                                                            @"albumPrice":@"$84.00"
                                                            }];
    
   
    // Do any additional setup after loading the view from its nib.
    [self customizeNavigationBar];
    [self.allItemsTableViewCell registerNib:[UINib nibWithNibName:@"MSPurchaserAllItemsTableViewCell" bundle:nil] forCellReuseIdentifier:purchaserAllItemsTableViewCellIdentifier];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:NO];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MSPurchaserAllItemsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:purchaserAllItemsTableViewCellIdentifier];
     cell.albumName.text = [dict objectForKey:@"albumName"];
    cell.albumPrice.text = [dict objectForKey:@"albumPrice"];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    MSPurchaserItemDetailsViewController *purchaserItemDetailsViewController = [[MSPurchaserItemDetailsViewController alloc] initWithNibName:@"MSPurchaserItemDetailsViewController" bundle:nil];
    [self.navigationController pushViewController:purchaserItemDetailsViewController animated:YES];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 16;
}

-(void)homeButtonTapped{
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
}
-(void)searchButtonTapped{
    MSSearchViewController *searchViewController = [[MSSearchViewController alloc]initWithNibName:@"MSSearchViewController" bundle:nil];
    [self.navigationController pushViewController:searchViewController animated:YES];
}
-(void)basketButtonTapped{
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    MSCheckOutViewController *checkOutViewController = [[MSCheckOutViewController alloc] initWithNibName:@"MSCheckOutViewController" bundle:nil];
   [self.navigationController pushViewController:checkOutViewController animated:YES];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(homeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"logo_museum_colored"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 32.0f)];
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setFrame:CGRectMake(10.0f, 0.0f, 44.0f, 32.0f)];
    [searchBtn addTarget:self action:@selector(searchButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"search_bar_button"] forState:UIControlStateNormal];
    
    UIButton *basketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [basketBtn setFrame:CGRectMake(54.0f, 0.0f, 44.0f, 32.0f)];
    [basketBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [basketBtn setImage:[UIImage imageNamed:@"cart_bar_button"] forState:UIControlStateNormal];
    
    UIButton *badgeBtn = [[UIButton alloc]initWithFrame:CGRectMake(80, 0, 18.0, 18.0f)];
    [badgeBtn setBackgroundImage:[UIImage imageNamed:@"badge_icon"] forState:UIControlStateNormal];
    [badgeBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[MSBeaconDetector SharedInstance].basketArray count]] forState:UIControlStateNormal];
    [badgeBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [view addSubview:searchBtn];
    [view addSubview:basketBtn];
    [view addSubview:badgeBtn];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    [self.navigationItem setRightBarButtonItem:searchBarButton];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
