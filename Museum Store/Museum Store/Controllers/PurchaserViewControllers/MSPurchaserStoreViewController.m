//
//  MSPurchaserStoreViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.//

#import "MSPurchaserStoreViewController.h"
#import "MSHomeViewController.h"
#import "RESideMenu.h"
#import "MSCheckOutViewController.h"
#import "MSPurchaserTabbarController.h"
#import "UIScrollView+ScrollIndicator.h"
#import "MSPurchaserStoreCollectionViewCell.h"
#import "MSBeaconDetector.h"
#import "MSSearchViewController.h"
@interface MSPurchaserStoreViewController (){
    NSMutableDictionary* dict;
    int itemCount;
    UIButton *badgeBtn;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *allItemsButton;
@property (weak, nonatomic) IBOutlet UIButton *topItemsButton;

@end

@implementation MSPurchaserStoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"STORE NAME";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dict = [[NSMutableDictionary alloc]initWithDictionary:@{@"name":@"Monalisa",
                                                            @"price":@"$28.76"
                                                            
                                                            }];
    
    
    itemCount = 8;
    // Do any additional setup after loading the view from its nib.
    [self customizeNavigationBar];
    [self.scrollView setContentSize:CGSizeMake(5*768, 438)];
    [self.scrollView enableCustomScrollIndicatorsWithScrollIndicatorType:JMOScrollIndicatorTypePageControl positions:JMOHorizontalScrollIndicatorPositionBottom color:[UIColor whiteColor]];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MSPurchaserStoreCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MSPurchaserStoreCollectionViewCell"];
    
}
#pragma mark - Collection view data source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return itemCount;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MSPurchaserStoreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MSPurchaserStoreCollectionViewCell" forIndexPath:indexPath];
    cell.nameLabel.text = [dict objectForKey:@"name"];
    cell.priceLabel.text = [dict objectForKey:@"price"];
    if (indexPath.row<4) {
        cell.lockedImageView.hidden = YES;
        cell.addToCartImageView.hidden = NO;
        cell.AddToCartButton.hidden = NO;
    }else{
        cell.lockedImageView.hidden = NO;
        cell.addToCartImageView.hidden = YES;
        cell.AddToCartButton.hidden =YES;
    }
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [cell addGestureRecognizer:tapRecognizer];
    cell.cellBlock = ^(id cell){
        [[MSBeaconDetector SharedInstance].basketArray addObject:@(indexPath.row)];
    };
    
    return cell;
}
- (void) handleTapFrom: (UITapGestureRecognizer *)tapRecognizer
{
    //Code to handle the gesture
    
    NSIndexPath* tappedCellPath = [self.collectionView indexPathForItemAtPoint:[tapRecognizer locationInView:self.collectionView]];
    MSPurchaserStoreCollectionViewCell *cell = (MSPurchaserStoreCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:tappedCellPath];
    if(tappedCellPath) {
        UICollectionViewCell *tappedCell = [self.collectionView cellForItemAtIndexPath:tappedCellPath];
        CGPoint tapInCellPoint = [tapRecognizer locationInView:tappedCell];
        //if the tap was outside of the cell bounds then its in negative values and it means the delete button was tapped
        if ( CGRectContainsPoint( CGRectMake(8, 209, 149, 29), tapInCellPoint ) ) {
            // inside
            [UIView animateWithDuration: 0.045 delay: 0 options: UIViewAnimationOptionCurveLinear
                             animations: ^{
                                 cell.AddToCartButton.alpha = 0.1;
                             }
                             completion: ^(BOOL finished) {
                                 [UIView animateWithDuration: 0.045 delay: 0 options: UIViewAnimationOptionCurveLinear
                                                  animations: ^{
                                                      cell.AddToCartButton.alpha = 1;
                                                  }completion: ^(BOOL finished) {}];
                             }];
            
            [[MSBeaconDetector SharedInstance].basketArray addObject:@(1)];
            [badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[MSBeaconDetector SharedInstance].basketArray count]] forState:UIControlStateNormal];
        } else {
            // outside
        }
        
    }
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//     [[MSBeaconDetector SharedInstance].basketArray addObject:@(indexPath.row)];
//    [badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[MSBeaconDetector SharedInstance].basketArray count]] forState:UIControlStateNormal];
}

- (IBAction)allItemsButtonTapped:(id)sender {
    itemCount = 16;
    [self.allItemsButton setSelected:YES];
    [self.topItemsButton setSelected:NO];
    [self.collectionView reloadData];
}
- (IBAction)topItemsButtonTapped:(id)sender {
    itemCount = 8;
    [self.allItemsButton setSelected:NO];
    [self.topItemsButton setSelected:YES];
    [self.collectionView reloadData];
}

-(void)homeButtonTapped{
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
}
-(void)searchButtonTapped{
    MSSearchViewController *searchViewController = [[MSSearchViewController alloc]initWithNibName:@"MSSearchViewController" bundle:nil];
    [self.navigationController pushViewController:searchViewController animated:YES];
}
-(void)basketButtonTapped{
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    MSCheckOutViewController *checkOutViewController = [[MSCheckOutViewController alloc] initWithNibName:@"MSCheckOutViewController" bundle:nil];
    [self.navigationController pushViewController:checkOutViewController animated:YES];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(homeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"logo_museum_colored"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 32.0f)];
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setFrame:CGRectMake(10.0f, 0.0f, 44.0f, 32.0f)];
    [searchBtn addTarget:self action:@selector(searchButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"search_bar_button"] forState:UIControlStateNormal];
    
    UIButton *basketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [basketBtn setFrame:CGRectMake(54.0f, 0.0f, 44.0f, 32.0f)];
    [basketBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [basketBtn setImage:[UIImage imageNamed:@"cart_bar_button"] forState:UIControlStateNormal];
    
    badgeBtn = [[UIButton alloc]initWithFrame:CGRectMake(80, 0, 18.0, 18.0f)];
    [badgeBtn setBackgroundImage:[UIImage imageNamed:@"badge_icon"] forState:UIControlStateNormal];
    [badgeBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[MSBeaconDetector SharedInstance].basketArray count]] forState:UIControlStateNormal];
    [badgeBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [view addSubview:searchBtn];
    [view addSubview:basketBtn];
    [view addSubview:badgeBtn];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    [self.navigationItem setRightBarButtonItem:searchBarButton];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
