//
//  MSProfileViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.//

#import "MSProfileViewController.h"
#import "MSProfileCollectionViewCell.h"
#import "MSHomeViewController.h"
#import "RESideMenu.h"
#import "MSCheckOutViewController.h"
#import "MSPurchaserTabbarController.h"
#import "MSBeaconDetector.h"
#import "MSProfileEditViewController.h"
@interface MSProfileViewController (){
    NSMutableDictionary *dict;
    NSString* firstNameText;
    NSString* lastNameText;
    NSString* locationText;
     NSString* aboutmeText;
}
@property (weak, nonatomic) IBOutlet UICollectionView *recentlyViewCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *beaconCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *artCollectorType;
@property (weak, nonatomic) IBOutlet UILabel *locationlabel;

@property (strong, nonatomic) MSProfileEditViewController *profileEditController;

@end

#define kFirstName        @"firstName"
#define kLastName         @"lastName"
#define kEmailAddress     @"emailAddress"

@implementation MSProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"MY PROFILE";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dict = [[NSMutableDictionary alloc]initWithDictionary:@{@"location":@"Bay Area, San Fransisco, CA",
                                                            @"artCollectorType":@"Modern art collector"
                                                            }];
    
    
    // Do any additional setup after loading the view from its nib.
    [self customizeNavigationBar];
     firstNameText = [[NSUserDefaults standardUserDefaults]objectForKey:kFirstName];
//     lastNameText = [[NSUserDefaults standardUserDefaults]objectForKey:kLastName];
     locationText = [[NSUserDefaults standardUserDefaults]objectForKey:@"location"];
    aboutmeText = [[NSUserDefaults standardUserDefaults]objectForKey:@"aboutme"];
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@",firstNameText];
    if (locationText.length>0) {
        self.locationlabel.text = locationText;
    }else{
        self.locationlabel.text = [dict objectForKey:@"location"];
    }
    if (aboutmeText.length>0) {
        self.artCollectorType.text = aboutmeText;
    }else{
        self.artCollectorType.text = [dict objectForKey:@"artCollectorType"];
    }
    [self.recentlyViewCollectionView registerNib:[UINib nibWithNibName:@"MSProfileCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MSProfileCollectionViewCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beaconAdded:) name:@"beaconNotification" object:nil];
    [self beaconAdded:nil];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)beaconAdded:(NSNotification *)Notification
{
    int  beaconCount = [MSBeaconDetector SharedInstance].currentUser.beacons.count;
    self.scoreLabel.text = [@(beaconCount*20) stringValue];
    self.beaconCountLabel.text = [@(beaconCount) stringValue];
    
    
}
#pragma mark - Collection view data source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 16;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MSProfileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MSProfileCollectionViewCell" forIndexPath:indexPath];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(homeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"logo_museum_colored"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 32.0f)];
    UIButton *basketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [basketBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [basketBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [basketBtn setImage:[UIImage imageNamed:@"cart_bar_button"] forState:UIControlStateNormal];
    
    UIButton *badgeBtn = [[UIButton alloc]initWithFrame:CGRectMake(26, 0, 18.0, 18.0f)];
    [badgeBtn setBackgroundImage:[UIImage imageNamed:@"badge_icon"] forState:UIControlStateNormal];
    [badgeBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[MSBeaconDetector SharedInstance].basketArray count]] forState:UIControlStateNormal];
    [badgeBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    
    UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [editBtn setFrame:CGRectMake(50.0f, 0.0f, 44.0f, 32.0f)];
    [editBtn addTarget:self action:@selector(editButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
    [editBtn setTitleColor:[UIColor colorWithRed:170/255.0 green:19/255.0 blue:81/255.0 alpha:1] forState:UIControlStateNormal];
    [view addSubview:editBtn];
    [view addSubview:basketBtn];
    [view addSubview:badgeBtn];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    [self.navigationItem setRightBarButtonItem:searchBarButton];
}
-(void)homeButtonTapped{
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
}
-(void)basketButtonTapped{
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    MSCheckOutViewController *checkOutViewController = [[MSCheckOutViewController alloc] initWithNibName:@"MSCheckOutViewController" bundle:nil];
    [self.navigationController pushViewController:checkOutViewController animated:YES];
}
-(void)editButtonTapped{
    //246 490
    self.profileEditController = [[MSProfileEditViewController alloc]initWithNibName:@"MSProfileEditViewController" bundle:nil];
   
    self.profileEditController.view.frame = CGRectMake(123, 245, self.profileEditController.view.frame.size.width, self.profileEditController.view.frame.size.height);
    self.profileEditController.firstNameField.text =firstNameText;
    self.profileEditController.locationField.text =locationText;
    self.profileEditController.collectorField.text =aboutmeText;
    
    UIView* view = [[UIView alloc]initWithFrame:self.view.bounds];
    view.backgroundColor =[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    [self.view addSubview:view];
    [view addSubview:self.profileEditController.view];
    
   UIButton *cancelButton = (UIButton*)[self.profileEditController.view viewWithTag:5555];
    
    [cancelButton addTarget:self.profileEditController action:@selector(cancelTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    __weak typeof(self) weakSelf = self;
    
    self.profileEditController.cancelBlock = ^(NSMutableDictionary* obj){
        NSString* firstName = [obj objectForKey:@"first_name"];
        NSString* lastName = [obj objectForKey:@"last_name"];
        NSString* location = [obj objectForKey:@"location"];
        NSString* collectionArt = [obj objectForKey:@"collector"];
        if (firstName.length>0 || lastName.length>0) {
            weakSelf.nameLabel.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@ %@",firstName,lastName] forKey:kFirstName];
        }
        
        if (location.length>0) {
             weakSelf.locationlabel.text = location;
            [[NSUserDefaults standardUserDefaults]setObject:location forKey:@"location"];
        }
        if (collectionArt.length>0) {
           weakSelf.artCollectorType.text = collectionArt;
            [[NSUserDefaults standardUserDefaults]setObject:collectionArt forKey:@"aboutme"];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        [view removeFromSuperview];
        weakSelf.profileEditController = nil;
        
    };
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
