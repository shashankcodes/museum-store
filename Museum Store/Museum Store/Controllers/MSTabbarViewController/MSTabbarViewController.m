//
//  MSTabbarViewController.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSTabbarViewController.h"
#import "KSProgressBar.h"
#import <MediaPlayer/MediaPlayer.h>
#import "MSBeaconDetector.h"

#define kCustomTabBarTag 4612
#define kTabBarItemLabelTag 389
#define widthTabBar 768


@interface MSTabbarViewController ()

@property (strong, nonatomic) MSVolumeView*volumeView;

@property (strong, nonatomic) UIButton *progressView;
@property (strong, nonatomic) KSProgressBar *progressBar;
@property (strong, nonatomic) UIButton *pointsButton;
@property (strong, nonatomic) UILabel *progressLabel;

@end

@implementation MSTabbarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupCustomTabBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beaconAdded:) name:@"beaconNotification" object:nil];
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)beaconAdded:(NSNotification *)Notification
{
    int  beaconCount = [MSBeaconDetector SharedInstance].currentUser.beacons.count;
   	[self.pointsButton setTitle:[NSString stringWithFormat:@"%d POINTS",beaconCount*20 ] forState:UIControlStateNormal];
    [self.progressBar setProgressValue:beaconCount/16.0f];
    [self.progressLabel setText:[NSString stringWithFormat:@"%d/%@ EXHIBITS",beaconCount,@(16)]];
    
}


- (void)setupCustomTabBar
{
	//custom tab bar view
	UIView *customTabBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, widthTabBar, 56.0)];
	customTabBarView.tag = kCustomTabBarTag;
	//set custom tab bar bg image
	UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tab_bar_background.png"]];
	bgImageView.frame = CGRectMake(0.0, 0.0, widthTabBar, 56.0);
	[customTabBarView addSubview:bgImageView];
	
	//add listen button to custom tab bar
    
    
    UIButton *pointsButton = [UIButton buttonWithType:UIButtonTypeCustom];
	pointsButton.frame = CGRectMake(0.0, 0.0, 176.0, 56.0);
	pointsButton.tag = 10;
	[pointsButton setImage:[UIImage imageNamed:@"points_icon.png"] forState:UIControlStateSelected];
	[pointsButton setImage:[UIImage imageNamed:@"points_icon.png"] forState:UIControlStateHighlighted];
	[pointsButton setImage:[UIImage imageNamed:@"points_icon.png"] forState:UIControlStateNormal];
    
    [pointsButton setTitleColor:[UIColor colorWithRed:102.0/255.0 green:185.0/255.0 blue:54.0/255.0 alpha:1] forState:UIControlStateNormal];
    [pointsButton setTitleColor:[UIColor colorWithRed:102.0/255.0 green:185.0/255.0 blue:54.0/255.0 alpha:1] forState:UIControlStateSelected];
    
	pointsButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, -15.0, 5.0, 0.0);

    //[pointsButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
	[customTabBarView addSubview:pointsButton];
    self.pointsButton = pointsButton;
    
    UIButton *gameButton = [UIButton buttonWithType:UIButtonTypeCustom];
	gameButton.frame = CGRectMake(176, 0, 104.0, 53.0);
	gameButton.tag = 11;
    [gameButton setImage:[UIImage imageNamed:@"game_icon_selected.png"] forState:UIControlStateSelected];
	[gameButton setImage:[UIImage imageNamed:@"game_icon_selected.png"] forState:UIControlStateHighlighted];
	[gameButton setImage:[UIImage imageNamed:@"game_icon.png"] forState:UIControlStateNormal];
    
    gameButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 38.0, 18.0, 0.0);
    [gameButton setTitleEdgeInsets:UIEdgeInsetsMake(35.0, -3, 0, 18)];
    [gameButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    
   // [gameButton setBackgroundColor:[UIColor redColor]];
     [gameButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
	[customTabBarView addSubview:gameButton];
    [gameButton setSelected:YES];
    
    [gameButton setTitle:@"GAME" forState:UIControlStateNormal];
    
    UIButton *exhibitButton = [UIButton buttonWithType:UIButtonTypeCustom];
	exhibitButton.frame = CGRectMake(280, 0, 104.0,53);
	exhibitButton.tag = 12;
	[exhibitButton setImage:[UIImage imageNamed:@"all_items_selected.png"] forState:UIControlStateSelected];
	[exhibitButton setImage:[UIImage imageNamed:@"exhibit_tab_selected.png"] forState:UIControlStateHighlighted];
	[exhibitButton setImage:[UIImage imageNamed:@"exhibit_tab.png"] forState:UIControlStateNormal];
    exhibitButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 38.0, 18.0, 0.0);
    [exhibitButton setTitleEdgeInsets:UIEdgeInsetsMake(35.0, -3, 0, 18)];
    [exhibitButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
     [exhibitButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
    [exhibitButton setTitle:@"EXHIBIT" forState:UIControlStateNormal];
    [customTabBarView addSubview:exhibitButton];
    
	//add me button to custom tab bar
	UIButton *mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
	mapButton.frame = CGRectMake(384.0, 0.0, 104.0, 53.0);
    mapButton.tag = 13;
    [mapButton setImage:[UIImage imageNamed:@"map_icon_selected.png"] forState:UIControlStateSelected];
	[mapButton setImage:[UIImage imageNamed:@"map_icon_selected.png"] forState:UIControlStateHighlighted];
	[mapButton setImage:[UIImage imageNamed:@"map_tab.png"] forState:UIControlStateNormal];
    
    [mapButton setTitle:@"MAP" forState:UIControlStateNormal];
    mapButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 38.0, 18.0, 0.0);
    [mapButton setTitleEdgeInsets:UIEdgeInsetsMake(35.0, 4, 0, 10)];
    [mapButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
	[customTabBarView addSubview:mapButton];
	 [mapButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
	//add stats button to custom tab bar
	UIButton *soundButton = [UIButton buttonWithType:UIButtonTypeCustom];	soundButton.frame = CGRectMake(488.0, 0.0, 104.0, 53.0);
    soundButton.tag = 14;
    [soundButton setImage:[UIImage imageNamed:@"sound_selected.png"] forState:UIControlStateSelected];
	[soundButton setImage:[UIImage imageNamed:@"sound_selected.png"] forState:UIControlStateHighlighted];
	[soundButton setImage:[UIImage imageNamed:@"sound_tab.png"] forState:UIControlStateNormal];
    [soundButton setTitle:@"SOUND" forState:UIControlStateNormal];
    soundButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 38.0, 18.0, 0.0);
    [soundButton setTitleEdgeInsets:UIEdgeInsetsMake(35.0, -3, 0, 18)];
    [soundButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
	[customTabBarView addSubview:soundButton];
	 [soundButton addTarget:self action:@selector(customTabBarAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *progressView = [UIButton buttonWithType:UIButtonTypeCustom];
	progressView.frame = CGRectMake(592, 0, 176.0,53);
	progressView.tag = 15;
	
    KSProgressBar *progressbarView = [[KSProgressBar alloc] initWithFrame:CGRectMake(56, 10, 102, 10)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(56, 30, 102, 13)];
    [label setTextAlignment:NSTextAlignmentCenter];
    label.font = [UIFont fontWithName:@"Helvetica" size:12];
    label.textColor = [UIColor whiteColor];
    self.progressBar = progressbarView;
    [progressView addSubview:progressbarView];
    [progressView addSubview:label];
    self.progressLabel = label;
    
    self.progressView = progressView;
   
    
    [customTabBarView addSubview:progressView];
    
    [self beaconAdded:nil];
    
	 //move all the four images up by 7.0 pixels
    
	
	MSTabbarViewController *tabBarController = self;
	UIView *tabBarControllerView = [tabBarController view];
	
	customTabBarView.frame = CGRectMake(0, tabBarControllerView.frame.size.height - 56, widthTabBar, 56.0);
	customTabBarView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
	
	[tabBarControllerView addSubview:customTabBarView];
	
	[self.tabBar setHidden:YES];
}

- (void)setButton:(UIButton *)button selected:(BOOL)selected
{
	[button setSelected:selected];
	[button setUserInteractionEnabled:!selected];
	
	
	if (selected)
	{
		
	}
	else
	{
		
	}
}

- (void)customTabBarAction:(UIButton *)sender
{
    [self setButton:sender selected:YES];
        if (sender.tag!=14)
        {
            [self setSelectedIndex:sender.tag - 10];
            
             [_volumeView removeFromSuperview];
           
            
        }
        else
        {
            if (!self.volumeView)
            {
                self.volumeView = [[MSVolumeView alloc] initWithFrame:CGRectMake(410, 900, 261, 64)];
                [self.volumeView setHidden:YES];
            }
           
            [[self.selectedViewController.view subviews] containsObject:self.volumeView] ? [self.volumeView setHidden:YES] :[self.volumeView setHidden:NO];
            
            [_volumeView.slider setThumbImage:[UIImage imageNamed:@"sound_thumb"] forState:UIControlStateNormal];
            [_volumeView.slider setMaximumTrackImage:[UIImage imageNamed:@"sound_full"] forState:UIControlStateNormal];
            [_volumeView.slider setMinimumTrackImage:[UIImage imageNamed:@"sound_blue"] forState:UIControlStateNormal];
            [_volumeView.slider addTarget:self action:@selector(volumeChanged:) forControlEvents:UIControlEventValueChanged];
            
            [self.volumeView removeFromSuperview];
            
            if (!self.volumeView.hidden)
            {
                [self.selectedViewController.view addSubview:_volumeView];
            }
          
            
            
            
            
            //TODO: Enter the Code to add View On existing Controller
            
        
    
    
	
	
        }
    
    for (int i = 10; i < 15; i++)
    {
        if (i != sender.tag)
        {
            UIView *buttonView = [self.view viewWithTag:i];
            if ([buttonView isKindOfClass:[UIButton class]])
            {
                [self setButton:(UIButton *)buttonView selected:NO];
            }
        }
        //if we switch to me tab fetch current user details
    }
    
}


-(void)volumeChanged:(UISlider*)sender
{
    MPMusicPlayerController *musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    musicPlayer.volume =  sender.value;
    
}


- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated
{
	UIView *customTabBar = [self.view viewWithTag:kCustomTabBarTag];
	
    if (hidden == customTabBar.hidden)
        return;
	
	if (animated)
	{
		customTabBar.hidden = NO;
        
		[UIView animateWithDuration:(animated?.3:.0) animations:^(){
			
            CGRect frame = customTabBar.frame;
			frame.origin.y = (hidden?self.view.bounds.size.height:(self.view.bounds.size.height-frame.size.height));
            customTabBar.frame = frame;
            self.tabBar.hidden = hidden;
            
		}completion:^(BOOL finished){
			
            customTabBar.hidden = hidden;
            self.tabBar.hidden = hidden;
		}];
	}
	else
	{
		customTabBar.hidden = hidden;
        self.tabBar.hidden = hidden;
	}
    
}
- (void)setTabBarHidden:(BOOL)hidden
{
    //    [self setTabBarHidden:hidden animated:NO];
    
    [self.tabBar setHidden:hidden];
    UIView *buttonView = [self.view viewWithTag:kCustomTabBarTag];
    [buttonView setHidden:hidden];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
