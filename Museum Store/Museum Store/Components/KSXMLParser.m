//
//  KSXMLParser.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "KSXMLParser.h"

@implementation KSXMLParser
-(void)parseXML:(NSString*)xmlString {
//    NSString *xmlString =
    
    NSData *xmlData = [xmlString dataUsingEncoding:NSASCIIStringEncoding];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
    
    [xmlParser setDelegate:self];
    
    [xmlParser parse];
    
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    if ([elementName  isEqual: @"PrintLetterBarcodeData"]) {
        _userDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:attributeDict];
    }
    self.currentElement=elementName;
    
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    
    self.currentElement=@"";
    
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{

}
@end
