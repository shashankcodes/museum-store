/*
 Copyright 2013 Scott Logic Ltd
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#import "SCViewController.h"
@import AVFoundation;
#import "SCShapeView.h"
#import "KSXMLParser.h"
#import "UIAlertView+Blocks.h"
#import "MSHomeViewController.h"
#import "RESideMenu.h"
#import "MSCheckOutViewController.h"
#import "MSPurchaserTabbarController.h"
#import "MSQRWebViewController.h"
#import "MSBeaconDetector.h"
#import "MSSearchViewController.h"
@interface SCViewController () <AVCaptureMetadataOutputObjectsDelegate> {
    AVCaptureVideoPreviewLayer *_previewLayer;
    SCShapeView *_boundingBox;
    NSTimer *_boxHideTimer;
    UILabel *_decodedMessage;
    BOOL isCaptured;
}
@property (strong,nonatomic)AVAudioPlayer* audioPlayer;
@property (strong, nonatomic) AVCaptureSession *captureSession;
@end

@implementation SCViewController

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self customizeNavigationBar];
    self.title = @"SCAN QR CODES";
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_background"]]];
    
    
    // Create a new AVCaptureSession
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:NO];
     [self startCapturing];
}

-(void)homeButtonTapped{
    MSHomeViewController *homeViewController = [[MSHomeViewController alloc]initWithNibName:@"MSHomeViewController" bundle:nil];
    UINavigationController *navRootController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [self.sideMenuViewController setContentViewController:navRootController animated:YES];
}
-(void)searchButtonTapped{
    MSSearchViewController *searchViewController = [[MSSearchViewController alloc]initWithNibName:@"MSSearchViewController" bundle:nil];
    [self.navigationController pushViewController:searchViewController animated:YES];
}
-(void)basketButtonTapped{
    [(MSPurchaserTabbarController*) self.sideMenuViewController.contentViewController setTabBarHidden:YES];
    MSCheckOutViewController *checkOutViewController = [[MSCheckOutViewController alloc] initWithNibName:@"MSCheckOutViewController" bundle:nil];
    [self.navigationController pushViewController:checkOutViewController animated:YES];
}
- (void)customizeNavigationBar
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.translucent = NO;
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(0.0f, 0.0f, 44.0f, 32.0f)];
    [menuBtn addTarget:self action:@selector(homeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:[UIImage imageNamed:@"logo_museum_colored"] forState:UIControlStateNormal];
    //    [menuBtn setImage:[UIImage imageNamed:@"sign_in_barbutton_highlighted"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuBtn];
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 32.0f)];
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setFrame:CGRectMake(10.0f, 0.0f, 44.0f, 32.0f)];
    [searchBtn addTarget:self action:@selector(searchButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"search_bar_button"] forState:UIControlStateNormal];
    
    UIButton *basketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [basketBtn setFrame:CGRectMake(54.0f, 0.0f, 44.0f, 32.0f)];
    [basketBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [basketBtn setImage:[UIImage imageNamed:@"cart_bar_button"] forState:UIControlStateNormal];
    
    UIButton *badgeBtn = [[UIButton alloc]initWithFrame:CGRectMake(80, 0, 18.0, 18.0f)];
    [badgeBtn setBackgroundImage:[UIImage imageNamed:@"badge_icon"] forState:UIControlStateNormal];
    [badgeBtn addTarget:self action:@selector(basketButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[[MSBeaconDetector SharedInstance].basketArray count]] forState:UIControlStateNormal];
    [badgeBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [view addSubview:searchBtn];
    [view addSubview:basketBtn];
    [view addSubview:badgeBtn];
    UIBarButtonItem *searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    [self.navigationItem setRightBarButtonItem:searchBarButton];
    
}
-(void) startCapturing
{
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    self.captureSession = session;
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    // Want the normal device
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    
    if(input) {
        // Add the input to the session
        [session addInput:input];
    } else {
        NSLog(@"error: %@", error);
        return;
    }
    isCaptured= NO;
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
    // Have to add the output before setting metadata types
    [session addOutput:output];
    // What different things can we register to recognise?
    // We're only interested in QR Codes
    [output setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    // This VC is the delegate. Please call us on the main queue
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    // Display on screen
    _previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _previewLayer.bounds =  CGRectMake(0, 0, 457, 457);
    _previewLayer.position = CGPointMake(CGRectGetMidX(self.view.bounds)-8, CGRectGetMidY(self.view.bounds)+57);
    [self.view.layer addSublayer:_previewLayer];
    
    
    
    // Add the view to draw the bounding box for the UIView
    _boundingBox = [[SCShapeView alloc] initWithFrame:self.view.bounds];
    _boundingBox.backgroundColor = [UIColor clearColor];
    _boundingBox.hidden = YES;
    [self.view addSubview:_boundingBox];
    
    [session startRunning];
    
}


#pragma mark - AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    
    
    for (AVMetadataObject *metadata in metadataObjects) {
        if (!isCaptured) {
            if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
                // Transform the meta-data coordinates to screen coords
                AVMetadataMachineReadableCodeObject *transformed = (AVMetadataMachineReadableCodeObject *)[_previewLayer transformedMetadataObjectForMetadataObject:metadata];
                // Update the frame on the _boundingBox view, and show it
                _boundingBox.frame = transformed.bounds;
                _boundingBox.hidden = NO;
                // Now convert the corners array into CGPoints in the coordinate system
                //  of the bounding box itself
                NSArray *translatedCorners = [self translatePoints:transformed.corners
                                                          fromView:self.view
                                                            toView:_boundingBox];
                
                // Set the corners array
                _boundingBox.corners = translatedCorners;
                
                // Update the view with the decoded text
                _decodedMessage.text = [transformed stringValue];
               
                
                isCaptured = YES;
                // Start the timer which will hide the overlay
                [self startOverlayHideTimer];
                
                [self.captureSession stopRunning];
                
                
                UIAlertView *alertView= [[UIAlertView alloc] initWithTitle:@" Details" message:[NSString stringWithFormat:@"%@",transformed.stringValue]
                                                               cancelTitle:@"Try Again" cancelBlock:^(NSArray *collectedStrings)
                                         {
                                             [self startCapturing];
                                         }
                                                                   okTitle:@"OK" okBlock:^(NSArray *collectedStrings)
                                         {
                                             MSQRWebViewController *webView = [[MSQRWebViewController alloc] initWithNibName:@"MSQRWebViewController" bundle:nil];
                                             
                                             
                                             webView.urlString = transformed.stringValue;
                                             
                                             [self.navigationController pushViewController:webView animated:YES];
                                             
                                             
                                         }];
                [alertView show];
                
                
                
                
                //            [self.navigationController pushViewController:createAccountViewController animated:NO];
                
            }
        }
        
    }
}

#pragma mark - Utility Methods
- (void)startOverlayHideTimer
{
    // Cancel it if we're already running
    if(_boxHideTimer) {
        [_boxHideTimer invalidate];
    }
    
    // Restart it to hide the overlay when it fires
    _boxHideTimer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                                     target:self
                                                   selector:@selector(removeBoundingBox:)
                                                   userInfo:nil
                                                    repeats:NO];
}

- (void)removeBoundingBox:(id)sender
{
    // Hide the box and remove the decoded text
    _boundingBox.hidden = YES;
    _decodedMessage.text = @"";
}

- (NSArray *)translatePoints:(NSArray *)points fromView:(UIView *)fromView toView:(UIView *)toView
{
    NSMutableArray *translatedPoints = [NSMutableArray new];

    // The points are provided in a dictionary with keys X and Y
    for (NSDictionary *point in points) {
        // Let's turn them into CGPoints
        CGPoint pointValue = CGPointMake([point[@"X"] floatValue], [point[@"Y"] floatValue]);
        // Now translate from one view to the other
        CGPoint translatedPoint = [fromView convertPoint:pointValue toView:toView];
        // Box them up and add to the array
        [translatedPoints addObject:[NSValue valueWithCGPoint:translatedPoint]];
    }
    
    return [translatedPoints copy];
}


@end
