//
//  KSXMLParser.h
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KSXMLParser : NSObject <NSXMLParserDelegate>

@property(strong,nonatomic)NSString *currentElement;
@property(strong,nonatomic)NSMutableDictionary *userDataDictionary;

-(void)parseXML:(NSString*)xmlString;
@end
