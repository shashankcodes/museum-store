//
//  MSVolumeView.m
//  MuseumStore
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import "MSVolumeView.h"

@implementation MSVolumeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.slider = [[UISlider alloc] initWithFrame:CGRectMake(10, 10, self.bounds.size.width-20      , self.bounds.size.height-30)];
        
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sound_background"]]];
        
        [self addSubview:self.slider];
        
        // Initialization code
    }
    return self;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
