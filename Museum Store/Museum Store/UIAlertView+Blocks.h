//
//  UIAlertView+Blocks.h
//  
//
//  Created by Sanjeev260191 on 5/17/14.
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.

#import <UIKit/UIKit.h>


@interface UIAlertView (Blocks) <UIAlertViewDelegate>

- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle cancelBlock:(void (^)(NSArray *collectedStrings))cancelBlock okTitle:(NSString *)okTitle okBlock:(void (^)(NSArray *collectedStrings))okBlock;

- (id)initWithTitle:(NSString *)title message:(NSString *)message style:(UIAlertViewStyle)style cancelTitle:(NSString *)cancelTitle cancelBlock:(void (^)(NSArray *collectedStrings))cancelBlock okTitle:(NSString *)okTitle okBlock:(void (^)(NSArray *collectedStrings))okBlock;

+ (void)quickAlertWithTitle:(NSString*)title message:(NSString*)message;

@end
