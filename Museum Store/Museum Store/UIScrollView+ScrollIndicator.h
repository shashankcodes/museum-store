//
//  UIScrollView+ScrollIndicator.h
//  CustomScrollViewIndicator
//
//  Created by Sanjeev260191 on 5/17/14. 
//  Copyright (c) 2014 TopCoder Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
  JMOVerticalScrollIndicatorPositionRight = 1 << 0, //Default for vertical
  JMOVerticalScrollIndicatorPositionLeft = 1 << 1,
  JMOHorizontalScrollIndicatorPositionBottom = 1 << 2, //Default for horizontal
  JMOHorizontalScrollIndicatorPositionTop = 1 << 3,
} JMOScrollIndicatorPosition;


typedef enum {
    JMOScrollIndicatorTypeClassic = 0, //Default
    JMOScrollIndicatorTypePageControl
} JMOScrollIndicatorType;

@interface UIScrollView (ScrollIndicator)

-(void)enableCustomScrollIndicatorsWithScrollIndicatorType:(JMOScrollIndicatorType)type positions:(JMOScrollIndicatorPosition)positions color:(UIColor *)color;
-(void)disableCustomScrollIndicator;

-(void)refreshCustomScrollIndicators;
-(void)refreshCustomScrollIndicatorsWithAlpha:(CGFloat)alpha;

@end
